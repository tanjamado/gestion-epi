USE epi_bdd;

--
-- Insertion des points de controles Rope Wrench (diffuseur)
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des flasques', 'Rope Wrench (diffuseur)', 'Visuelle'),
                                                                            ('Etat de la goupille', 'Rope Wrench (diffuseur)', 'Visuelle'),
                                                                            ('Etat de l\'écrou de connexion à la longe', 'Rope Wrench (diffuseur)', 'Visuelle'),
                                                                            ('Etat du réa et de son axe', 'Rope Wrench (diffuseur)', 'Visuelle'),
                                                                            ('Efficacité de l\'ergot de verouillage de la goupille', 'Rope Wrench (diffuseur)', 'Fonctionelle'),
                                                                            ('Rotation libre du réa', 'Rope Wrench (diffuseur)', 'Fonctionelle'),
                                                                            ('Test opérationnel avec noeud autobloquant et poulie', 'Rope Wrench (diffuseur)', 'Fonctionelle');

--
-- Insertion des points de controles Rope Wrench (longe)
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat de boucles de connexion', 'Rope Wrench (longe)', 'Visuelle'),
                                                                            ('Etat des coutures', 'Rope Wrench (longe)', 'Visuelle'),
                                                                            ('Présence et état de l\'anneau en caoutchouc', 'Rope Wrench (longe)', 'Visuelle'),
                                                                            ('Etat du raidisseur de longe', 'Rope Wrench (longe)', 'Visuelle'),
                                                                            ('Efficacité du raidisseur de longe', 'Rope Wrench (longe)', 'Fonctionnelle'),
                                                                            ('Efficacité de l\'anneau en caoutchouc', 'Rope Wrench (longe)', 'Fonctionnelle');


--
-- Insertion des points de controles Escaper
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat de la corde', 'Escaper', 'Visuelle'),
                                                                            ('Présence de la marque noire', 'Escaper', 'Visuelle'),
                                                                            ('Etat des coutures', 'Escaper', 'Visuelle'),
                                                                            ('Etat de l\'élastique de rappel', 'Escaper', 'Visuelle'),
                                                                            ('Etat des "sangles tunnel" (protection élastique)', 'Escaper', 'Visuelle'),
                                                                            ('Etat de la boucle d\'ancrage', 'Escaper', 'Visuelle'),
                                                                            ('Présence des marques d\'insertion', 'Escaper', 'Visuelle'),
                                                                            ('Efficacité de la tresse dyneema', 'Escaper', 'Fonctionelle'),
                                                                            ('Efficatité de l\'élastique de rappel', 'Escaper', 'Fonctionelle'),
                                                                            ('Test opérationnel', 'Escaper', 'Fonctionelle');

--
-- Insertion des points de controles Poulie Bloqueur
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (flasques)', 'Poulie Bloqueur', 'Visuelle'),
                                                                            ('Etat des emplacements de connexion', 'Poulie Bloqueur', 'Visuelle'),
                                                                            ('Etat des rivets', 'Poulie Bloqueur', 'Visuelle'),
                                                                            ('Etat de la gâchette (corps, dents...)', 'Poulie Bloqueur', 'Visuelle'),
                                                                            ('Etat du réa (gorge, axe de rotation)', 'Poulie Bloqueur', 'Visuelle'),
                                                                            ('Efficacité du système de verrouillage du flasque (si présent)', 'Poulie Bloqueur', 'Fonctionnelle'),
                                                                            ('Efficacité du ressort de rappel de la gâchette', 'Poulie Bloqueur', 'Fonctionnelle'),
                                                                            ('Rotation libre du réa', 'Poulie Bloqueur', 'Fonctionnelle'),
                                                                            ('Test opérationnel', 'Poulie Bloqueur', 'Fonctionnelle');

--
-- Insertion des points de controles Antichute Mobile
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (flasques)', 'Antichute Mobile' , 'Visuelle'),
                                                                            ('Etat des cames, mâchoires, gâchettes, galet', 'Antichute Mobile', 'Visuelle'),
                                                                            ('Etat des taquets (si présent)', 'Antichute Mobile', 'Visuelle'),
                                                                            ('Etat des rivets', 'Antichute Mobile', 'Visuelle'),
                                                                            ('Etat des emplacements de connexion', 'Antichute Mobile' , 'Visuelle'),
                                                                            ('Etat des axes de rotation et manilles (si présent)', 'Antichute Mobile', 'Visuelle'),
                                                                            ('Articulation libre des pièces mobiles', 'Antichute Mobile', 'Fonctionnelle'),
                                                                            ('Efficacité du système de verouillage des flasques (si présent)', 'Antichute Mobile', 'Fonctionnelle'),
                                                                            ('Efficacité des ressorts de rappel (si présent)', 'Antichute Mobile', 'Fonctionnelle'),
                                                                            ('Efficacité du système de blocage de l\'antichute', 'Antichute Mobile', 'Fonctionnelle'),
                                                                            ('Test opérationnel sur corde', 'Antichute Mobile', 'Fonctionnelle');

--
-- Insertion des points de controles Absorbeur
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat extérieur de l\'enveloppe', 'Absorbeur', 'Visuelle'),
                                                                            ('Etat de la sangle à déchirement', 'Absorbeur', 'Visuelle'),
                                                                            ('Etat des coutures', 'Absorbeur', 'Visuelle'),
                                                                            ('Etat du témoin de chute (si présent)', 'Absorbeur','Visuelle'),
                                                                            ('Etat des points de connexion', 'Absorbeur', 'Visuelle'),
                                                                            ('Etat des éléments de protection', 'Absorbeur', 'Visuelle');
--
-- Insertion des points de controle Longe EN 354
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat de la corde ou de la sangle', 'Longe EN 354', 'Visuelle'),
                                                                            ('Etat des boucles de connexion (épissures)', 'Longe EN 354', 'Visuelle'),
                                                                            ('Etat des coutures des boucles de connexion', 'Longe EN 354', 'Visuelle'),
                                                                            ('Etat des éléments de protection', 'Longe EN 354', 'Visuelle');

--
-- Insertion des points de controle Chicane
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps métallique (flasques du diffuseur)', 'Chicane', 'Visuelle'),
                                                                            ('Etat de la poignée et du trou de connexion', 'Chicane', 'Visuelle'),
                                                                            ('Etat de l\'axe de liaison poingée/diffuseur', 'Chicane', 'Visuelle'),
                                                                            ('Etat du serrage des axes de frottement', 'Chicane', 'Visuelle'),
                                                                            ('Comptabilité du connecteur de laision : section en H (si présent)', 'Chicane', 'Visuelle'),
                                                                            ('Efficacité du ressort de maintien en position d diffuseur', 'Chicane', 'Fonctionnelle'),
                                                                            ('Mouvement libre des pièces en mouvement', 'Chicane', 'Fonctionnelle'),
                                                                            ('Test opérationnel avec Zig Zag', 'Chicane', 'Fonctionnelle');

--
-- Insertion des points de controle Casque
--

INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat extérieur et intérieur de la calotte (usure, fissures, marques, déformation, brûlures, produit chimiques...)', 'Casque', 'Visuelle'),
                                                                            ('Etat de la coiffe, tour de tête, sangles, coutures, boucles de fermeture', 'Casque', 'Visuelle'),
                                                                            ('Etat des éléments de la coiffe, du tour de tête (clips, rivets, point d\'attache)', 'Casque','Visuelle'),
                                                                            ('Réglage tour de tête', 'Casque', 'Fonctionnelle'),
                                                                            ('Réglage avant/arrière de la jugulaire', 'Casque', 'Fonctionnelle'),
                                                                            ('Système d\'ouverture/fermeture de la jugulaire', 'Casque', 'Fonctionnelle');

--
-- Insertion des points de controle Harnais
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des sangles (coupure, usure, dommage divers, produits chimiques...)', 'Harnais', 'Visuelle'),
                                                                            ('Etat des coutures de sécurité (fils coupés, distendus, usés...)', 'Harnais', 'Visuelle'),
                                                                            ('Etat des anneaux d\'accrochage (déformation, usure, corrosion, marque...)', 'Harnais', 'Visuelle'),
                                                                            ('Etat du ou des ponts (sangle, coupure, brûlures, génie de protection, manilles...)', 'Harnais', 'Visuelle'),
                                                                            ('Etat des éléménets de confort (dosseret, cuissars, sellette, porte matériel, passants...)', 'Harnais', 'Visuelle'),
                                                                            ('Passage des sangles dans les boucles de fermeture', 'Harnais', 'Fonctionnelle'),
                                                                            ('Fonctionnement du bouclage automatique', 'Harnais', 'Fonctionnelle'),
                                                                            ('Fonctionnement des boucles de réglage', 'Harnais', 'Fonctionnelle');

--
-- Insertion des points de controle Connecteur
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (fissures, marques, déformation, usure, corrosion...)', 'Connecteur', 'Visuelle'),
                                                                            ('Etat du bec de fermeture (fissures, marques, déformation, usure, corrosion...)', 'Connecteur', 'Visuelle'),
                                                                            ('Etat du doigt de fermeture , du rivet et de la virole (fissures, marques, déformation, usure, corrosion...)', 'Connecteur', 'Visuelle'),
                                                                            ('Bon fonctionnement du doigt de fermeture (souffler et graisser au préalable)', 'Connecteur', 'Fonctionnelle'),
                                                                            ('Bon alignement doigt-bec', 'Connecteur', 'Fonctionnelle'),
                                                                            ('Efficacité du ressort de rappel et de l\'articulation du doigt de fermeture', 'Connecteur', 'Fonctionnelle'),
                                                                            ('Bon fonctionnement du système de verouillage', 'Connecteur', 'Fonctionnelle');

--
-- Insertion des points de controle Corde de rappel
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des terminaisons (engainage pour épissure rentrée, surliure, coutures pour épissures cousues)', 'Corde de rappel', 'Visuelle'),
                                                                            ('Etat des protections pour les épissures cousues (remplacer la gaine thermo-rétractable si nécessaire)', 'Corde de rappel', 'Visuelle'),
                                                                            ('Etat de la gaine (fuseaux sectionnés, altérés, cououre, usure, brûlure, zones pelucheuses...)', 'Corde de rappel', 'Visuelle'),
                                                                            ('Etat de l\'âme (hernies, dépressions, interruptions...)', 'Corde de rappel', 'Visuelle');

--
-- Insertion des points de controles Fausse Fourche
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des sangles (coupure, usure, dommage divers, produits chimiques...)', 'Fausse Fourche', 'Visuelle'),
                                                                            ('Etat des coutures de sécurité (fils coupés, distendus, usés...)', 'Fausse Fourche', 'Visuelle'),
                                                                            ('Etat des anneaux d\'accrochage (déformation, usure, corrosion, marque...)', 'Fausse Fourche', 'Visuelle');

--
-- Insertion des points de controles Fausse Fourche à poulies
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des sangles (coupure, usure, dommage divers, produits chimiques...)', 'Fausse Fourche à poulies', 'Visuelle'),
                                                                            ('Etat des coutures de sécurité (fils coupés, distendus, usés...)', 'Fausse Fourche à poulies', 'Visuelle'),
                                                                            ('Etat des anneaux d\'accrochage (déformation, usure, corrosion, marque...)', 'Fausse Fourche à poulies', 'Visuelle'),
                                                                            ('Etat des flasques et du réa de la poulie : (déformation flasques, jeu de l’axe, corrosion, marques,…)', 'Fausse Fourche à poulies', 'Visuelle'),
                                                                            ('Etat du tendeur réducteur et de ses éléments de friction : (came, flasques ...)', 'Fausse Fourche à poulies', 'Visuelle');


--
-- Insertion des points de controles Fausse Fourche en cordes avec noeud autobloquant
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des sangles (coupure, usure, dommage divers, produits chimiques...)', 'Fausse Fourche en cordes avec noeud autobloquant', 'Visuelle'),
                                                                            ('Etat des coutures de sécurité (fils coupés, distendus, usés...)', 'Fausse Fourche en cordes avec noeud autobloquant', 'Visuelle'),
                                                                            ('Etat des anneaux d\'accrochage (déformation, usure, corrosion, marque...)', 'Fausse Fourche en cordes avec noeud autobloquant', 'Visuelle'),
                                                                            ('Etat des terminaisons : (engainage pour épissure rentrée, surliure, coutures pour épissures cousues)', 'Fausse Fourche en cordes avec noeud autobloquant', 'Visuelle'),
                                                                            ('Etat de la gaine : (fuseaux sectionnés, altérés, coupure, usure, brûlure, zones pelucheuses,…)', 'Fausse Fourche en cordes avec noeud autobloquant', 'Visuelle'),
                                                                            ('Etat de l’âme : (hernies, dépressions, interruptions,…)', 'Fausse Fourche en cordes avec noeud autobloquant', 'Visuelle');

--
-- Insertion des points de controles Longe de Maintien
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des terminaisons (engainage pour ép. rentrée, surliure, coutures pour cousue', 'Longe de Maintien', 'Visuelle'),
                                                                            ('Etat des protections pour les épissures cousues (remplacer la gaine thermo-rétractable si nécessaire)', 'Longe de Maintien', 'Visuelle'),
                                                                            ('Etat de la gaine (fuseaux sectionnées, altérés, coupure, zones pelucheuses...)', 'Longe de Maintien', 'Visuelle'),
                                                                            ('Etat de l\'âme (hernies, dépressions...)', 'Longe de Maintien', 'Visuelle'),
                                                                            ('Etat du câble acier (cas échéant)', 'Longe de Maintien', 'Visuelle'),
                                                                            ('Fonctionnement du tendeur-réducteur', 'Longe de Maintien', 'Fonctionnelle'),
                                                                            ('Etat des éléments de friction (came, flasques, corde du noeud, autobloquant...)', 'Longe de Maintien', 'Fonctionnelle'),
                                                                            ('Présence d\'élement d\'arrêt', 'Longe de Maintien', 'Fonctionnelle');

--
-- Insertion des points de controles Poignée ascensionnelle
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (fissures, marques, déformation, usure, corrosion...)', 'Poignée ascensionnelle', 'Visuelle'),
                                                                            ('Etat de la butée anti-retour (fissures, marques, déformation, usure, corrosion...)', 'Poignée ascensionnelle', 'Visuelle'),
                                                                            ('Etat de la gâchette (dents usées ou manquantes, corrosion), et de son rivet (fissure, marque, usure corrosion...)', 'Poignée ascensionnelle', 'Visuelle'),
                                                                            ('Etat du taquet de sécurité (axe-usure, fissure, état du polyamide...)', 'Poignée ascensionnelle', 'Visuelle'),
                                                                            ('Efficacité du ressort de rappel de la gâchette', 'Poignée ascensionnelle', 'Fonctionnelle'),
                                                                            ('Efficacité du ressort du taquet de sécurité', 'Poignée ascensionnelle', 'Fonctionnelle'),
                                                                            ('Ouverture complète du taquet de sécurité sur le corps du bloqueur', 'Poignée ascensionnelle', 'Fonctionnelle'),
                                                                            ('Coulissement vers le haut et blocaque vers le bas', 'Poignée ascensionnelle', 'Fonctionnelle');

--
-- Insertion des points de controles Bloqueur à gachêtte
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (fissures, marques, déformation, usure, corrosion)', 'Bloqueur à gachêtte', 'Visuelle'),
                                                                            ('Etat de la butée anti retour (fissures, marques, déformation, usure, corrosion...)', 'Bloqueur à gachêtte', 'Visuelle'),
                                                                            ('Etat de la gâchette (dents usées ou manquantes, corrosion), et de son rivet (fissure, marque, usure corrosion...)', 'Bloqueur à gachêtte', 'Visuelle'),
                                                                            ('Etat du taquet de sécurite (axe-usure, fissure, état du polyamide...)', 'Bloqueur à gachêtte', 'Visuelle'),
                                                                            ('Efficacité du ressort de rappel de la gâchette', 'Bloqueur à gachêtte', 'Fonctionnelle'),
                                                                            ('Efficacité du ressort du taquet de sécurité', 'Bloqueur à gachêtte', 'Fonctionnelle'),
                                                                            ('Ouverture complète du taquet de sécurité sur le corps du bloqueur', 'Bloqueur à gachêtte', 'Fonctionnelle'),
                                                                            ('Coulissement vers le haut et blocage vers le bas', 'Bloqueur à gachêtte', 'Fonctionnelle');

--
-- Insertion des points de controles Bloqueur à came
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (fissures, marques, déformation, usure, corrosion...)', 'Bloqueur à came', 'Visuelle'),
                                                                            ('Etat des pièces mobiles (came, axe de la came, goupille de verrouillage, ressort) (fissures, marques, déformation, usure, corrosion)', 'Bloqueur à came', 'Visuelle'),
                                                                            ('Etat de l\'encoche de verouillage (abscence de terre, de sable...)', 'Bloqueur à came', 'Visuelle'),
                                                                            ('Etat de la cordellete et du câble acier', 'Bloqueur à came', 'Visuelle'),
                                                                            ('Efficacité du ressort de la goupille de verouilage', 'Bloqueur à came', 'Fonctionnelle'),
                                                                            ('Coulissement vers le haut et blocage vers le bas sur la corde', 'Bloqueur à came', 'Fonctionnelle');

--
-- Insertion des points de controles Corde d'accès
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des terminaisons (engainage pour épissure rentrée, surliure, coutures pour épissures cousues)', 'Corde d\'accès', 'Visuelle'),
                                                                            ('Etat des protections pour les épissures cousues (remplacer la gine thermo-rétractable si nécessaire)', 'Corde d\'accès', 'Visuelle'),
                                                                            ('Etat de la gaine (fuseaux sectionnées, altérés, coupure, usure, brûlure, zones pelucheuses...)', 'Corde d\'accès', 'Visuelle'),
                                                                            ('Etat de l\'âme (hernies, dépressions, interruptions...)', 'Corde d\'accès', 'Visuelle');

--
-- Insertion des points de controles Poulie
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (fissures, marques, déformation, usure, corrosion...)', 'Poulie', 'Visuelle'),
                                                                            ('Etat des flasques (fixes ou mobiles) (fissures, marques, déformation, usure, corrosion...)', 'Poulie', 'Visuelle'),
                                                                            ('Etat des emplacements de connexion (fissures, marques, déformation, usure, corrosion...)', 'Poulie', 'Visuelle'),
                                                                            ('Etat de la gorges et de l\'axe du réa (usure, fissure, jeu, déformation...)', 'Poulie', 'Visuelle'),
                                                                            ('Rotation libre du réa', 'Poulie', 'Fonctionnelle'),
                                                                            ('Bonne ouverture-fermeture des flasques', 'Poulie', 'Fonctionnelle');

--
-- Insertion des points de controle Zigzag
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (fissures, marques, déformation, usure, corrosion...)', 'Zigzag', 'Visuelle'),
                                                                            ('Etat de la chaîne de blocage (pliage-dépliage libre, abscence de jeu dans les rivets...)', 'Zigzag', 'Visuelle'),
                                                                            ('Etat de la gorges et de l\'axe du réa (usure, fissure, déformation...)', 'Zigzag', 'Visuelle'),
                                                                            ('Absence de corps étranger dans la chaîne de blocage', 'Zigzag', 'Visuelle'),
                                                                            ('Mobilité du levier de déblocage et efficacité du ressort', 'Zigzag', 'Fonctionnelle'),
                                                                            ('Rotation libre du réa et abscence de jeu sur l\'axe', 'Zigzag', 'Fonctionnelle'),
                                                                            ('Bon fonctionnement de l\'émerillon (pivotement)', 'Zigzag', 'Fonctionnelle'),
                                                                            ('Coulissement vers le haut et blocage vers le bas', 'Zigzag', 'Fonctionnelle');
--
-- Insertion des points de controle Emerillon
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps et des pièces mobiles (fissures, marques, déformation, usure, corrosion...)', 'Emerillon', 'Visuelle'),
                                                                            ('Etat des trous de connexion (fissures, marques, déformation, usure, corrosion...)', 'Emerillon', 'Visuelle'),
                                                                            ('Etat du rivet de jonction des deux parties mobiles (fissures, marques, déformation, usure, corrision...)', 'Emerillon', 'Visuelle'),
                                                                            ('Vérification de la rotation libre des deux pièces l\'une par rapport à l\'autre', 'Emerillon', 'Fonctionnelle');

--
-- Insertion des points de controle Pont (Point central)
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat de la sangle ou de la corde (coupure, usure, pdt chimiques, fuseaux sectionnées, altérés, brûlure, zones pelucheuses, homogénéité de l\'âme, hernie de la gaine...)', 'Pont (Point central)', 'Visuelle'),
                                                                            ('Etat des coutures de sécurité (fils coupés, distendus, usés...)', 'Pont (Point central)', 'Visuelle'),
                                                                            ('Etat des anneaux d\'accrochage (anneaux ouvrables, manilles lyres...)', 'Pont (Point central)', 'Visuelle'),
                                                                            ('Etat des terminaisons (engainage pour épissure rentrée, surliure, coutures pour épissures cousues)', 'Pont (Point central)', 'Visuelle'),
                                                                            ('Conformité du montage (se reporter à la notice constructeur)', 'Pont (Point central)', 'Fonctionnelle'),
                                                                            ('Etat du tendeur réducteur et de ses éléments de friction (came, flasques...)', 'Pont (Point central)', 'Fonctionnelle');

--
-- Insertion des points de controle Anneau de sangle
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des sangles (coupures, usures, produits chimiques, brûlures...)', 'Anneau de sangle', 'Visuelle'),
                                                                            ('Etat des coutures de sécurité (fil coupés, distendus, usés...)', 'Anneau de sangle', 'Visuelle'),
                                                                            ('Etat des boucles de réglages (si présentes) (déformation, usures, corrosion, marques, fissures...)', 'Anneau de sangle', 'Visuelle'),
                                                                            ('Contrôle du fonctionnement de la boucle de réglage (si présente)', 'Anneau de sangle', 'Fonctionnelle');

--
-- Insertion des points de controle Descendeur
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat du corps (fissures, marques, déformation, usure, corrosion...)', 'Descendeur', 'Visuelle'),
                                                                            ('Etat des cames et contre-cames (usure excessive, controle des témoins d\'usure si présents, mobilité)', 'Descendeur', 'Visuelle'),
                                                                            ('Etat de la (ou des) flasque(s) (usure, jeux, fissures, déformation...)', 'Descendeur', 'Visuelle'),
                                                                            ('Contrôle du bouton de bloquage de la flasque' ,'Descendeur', 'Visuelle'),
                                                                            ('Contrôle des éléments de verrouillage', 'Descendeur', 'Visuelle'),
                                                                            ('Abscence de corps étrangers dans l\'appareil', 'Descendeur', 'Visuelle'),
                                                                            ('Contrôle de levier de commande de la came', 'Descendeur', 'Fonctionnelle'),
                                                                            ('Contrôle du système anti-panique (si présent)', 'Descendeur', 'Fonctionnelle'),
                                                                            ('Blocage vers le bas du descendeur', 'Descendeur', 'Fonctionnelle');

--
-- Insertion des points de controle Noeud autobloquant
--
INSERT INTO point_de_controle (libelle, type_epi, type_verification) VALUES ('Etat des terminaisons (Engainage pour épissure rentrée, surliure, coutures pour épissures cousue)', 'Noeud autobloquant', 'Visuelle'),
                                                                            ('Etat des protections pour les épissures cousues (Remplacer la gaine thermo-rétractable si nécessaire)', 'Noeud autobloquant', 'Visuelle'),
                                                                            ('Etat de la gaine (Fuseaux sectionnées, altérés, coupure, usure, brûlure, zones pelucheuses...)', 'Noeud autobloquant', 'Visuelle'),
                                                                            ('Etat de l\'âme (Hernies, dépressions, interruptions...)', 'Noeud autobloquant', 'Visuelle');
