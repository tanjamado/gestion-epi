USE epi_bdd;

--
-- Insertion de groupes
--
INSERT INTO groupe (nom_groupe)
VALUES ('Formateurs'),
       ('Matériel Collectif'),
       ('Stages courts'),
       ('Région'),
       ('Cordistes'),
       ('Stock');

INSERT INTO lot (nom_lot, code_couleur, groupe) VALUE ('Stock', 'Sans Couleur', 'Stock');

--
-- Insertion de type d'EPI
--
INSERT INTO type_epi (libelle)
VALUES ('Casque'),
       ('Harnais'),
       ('Pont (Point central)'),
       ('Connecteur'),
       ('Corde de rappel'),
       ('Corde d\'accès'),
       ('Noeud autobloquant'),
       ('Poulie'),
       ('Zigzag'),
       ('Fausse fourche'),
       ('Fausse fourche à poulies'),
       ('Fausse fourche en cordes avec noeud autobloquant'),
       ('Bloqueur à gachêtte'),
       ('Bloqueur à came'),
       ('Poignée ascensionnelle'),
       ('Emerillon'),
       ('Descendeur'),
       ('Rope wrench (diffuseur)'),
       ('Rope wrench (longe)'),
       ('Chicane'),
       ('Escaper'),
       ('Poulie bloqueur'),
       ('Antichute mobile'),
       ('Longe de Maintien'),
       ('Longe EN 354'),
       ('Anneau de Sangle'),
       ('Absorbeur');

COMMIT;