# Gestion EPI

## Réalisation d'un site web de gestion des EPI

Réalisé par : 
 - Anas Moncef
 - Emeric Pain
 - Fannie Le Fel
 - Johann André
 - Kilian Paquier
 - Paul Retail
 - Steven Martin
 - Yongda Lin

Vous trouverez toute la documentation pour la reprise du projet aux liens ci dessous :

Fiche de reprise du projet :
https://docs.google.com/document/d/1JIzNs1yDWI7jrVBGoH6Cwon3XVG1QspcQMnhluGC24M/edit?usp=sharing

Fiche du projet et fonctionnalités :
https://docs.google.com/document/d/1Yq5dzExO4buasvqioMryL4QDKnzLRrpEWakwHYIK1ig/edit?usp=sharing

Guide utilisateur :
https://docs.google.com/document/d/15qzM3U1LFGMLi-6ewViR2ocn1vVl-Li0UUmBBCwHwbg/edit?usp=sharing

Spécifications :
https://docs.google.com/document/d/1SUOxdR6HKU9kcY35j-VKa4LAd1NT_l7WHS0kgRlNoeU/edit?usp=sharing