<?php
/**
 * Created by PhpStorm.
 * User: Steven
 * Date: 07/05/2019
 * Time: 15:55
 */

class Notices extends CI_Controller
{
    public function index()
    {
        $this->load->helper('url');
        require('./scripts_requests/verify_session.php');
        $this->load->view('php/notices');
    }
}