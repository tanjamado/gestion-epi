<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Tableau des EPI</title>

    <?php include("head.php"); ?>
    <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>-->
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

</head>

<?php include("nav.php"); ?>

<body>

<div class="container pt-5 animated fadeIn mt-5 mb-5 bg-light">
    <div class="container">
        <div class="row pb-2">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h3 class="panel-title">Visualisation du lot</h3>
            </div>
        </div>
        <div class="border border-light pr-5 pl-5 pb-3">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label for="structure" content="center">Groupe</label>
                    <select class="browser-default custom-select mb-4" id="structure">

                    </select>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <label for="lot">Lot</label>
                    <select class="browser-default custom-select mb-4" id="lot">

                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-12" id="exportPDF">
                    <table id="table-epi"
                           class="table table-hover table-responsive-lg table-striped w-100 display">
                        <thead>
                        <tr>
                            <th>Type d'EPI</th>
                            <th>Identification individuelle</th>
                            <th>Marque</th>
                            <th>Date de fin de vie</th>
                            <th>Dernière vérification</th>
                            <th>État</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <p id="success"></p>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 float-right">
                        <button type="button" id="modalButton" class="btn special-color btn-block text-white my-4" data-toggle="modal"
                                data-target="#exampleModalPreview">Envoyer un EPI au stock
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<div class="modal fade fadeInDown" id="exampleModalPreview" tabindex="-1"
     aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content pt-5 pl-5 pr-5">
            <div class="header animated zoomIn" align="center">
                <h3 class="panel-title mb-3">Envoi d'un EPI au stock</h3>
            </div>
            <div class="contentText animated zoomIn">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <label for="epi">EPI à stocker *</label>
                            <select class="browser-default custom-select" id="epi" required>
                                <option hidden value="">Choisir l'EPI...</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="row">
                        <div class="col-12">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <button class="btn special-color btn-block text-white mt-4 mb-3"
                                        id="submitButton" type="submit">
                                    Mettre au stock
                                </button>
                            </div>
                        </div>
                        <div class="col-12 mb-2">
                            <p id="errorMessage"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/dotation.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.1.1/jspdf.plugin.autotable.min.js"></script>

</html>