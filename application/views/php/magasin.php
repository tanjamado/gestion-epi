<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Stock</title>

    <?php include("head.php"); ?>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.css"/>

</head>

<?php include("nav.php"); ?>

<body>
<div class="container pt-5 animated fadeIn mt-5 mb-5 bg-light">
    <div class="container">
        <div class="row pb-2">
            <div class="col-12">
                <h3 class="panel-title">Gestion du stock</h3>
            </div>
        </div>
        <div class="border border-light pr-5 pl-5 pb-3">
            <div class="row">
                <div class="col-12">
                    <table id="table-epi"
                           class="table table-hover table-responsive-lg table-striped w-100 display">
                        <thead>
                        <tr>
                            <th>Type d'EPI</th>
                            <th>Identification individuelle</th>
                            <th>Marque</th>
                            <th>Date de fin de vie</th>
                            <th>Date de vérification</th>
                            <th>État</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <p id="success"></p>
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<div class="modal fade fadeInDown" id="exampleModalPreview" tabindex="-1"
     aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content pb-4 pt-4" align="center">
            <div class="modal-header mt-n4">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="header animated zoomIn" align="center">
                <h3 class="panel-title">Ajout d'un EPI à un lot</h3>
            </div>
            <div class="contentText animated zoomIn pr-4 pt-4 pl-4">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <p id="EPI_ID"><b>Numéro de série de l'EPI : </b></p>
                            <p id="modele"><b>Modèle : </b></p>
                            <p><b>A quel lot souhaitez-vous ajouter l'EPI ?</b></p>
                            <select class="browser-default custom-select mb-3" id="btnLot">
                                <option hidden disabled="" selected="">Choix du lot</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <div class="row">
                        <div class="col-12">
                            <div class="col-xs-12 mb-n4 col-sm-12 col-md-6 col-lg-6 float-right">
                                <button class="btn special-color btn-block text-white my-4"
                                        id="submitButton" type="submit">
                                    Ajouter
                                </button>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <p id="errorMessage"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/magasin_change_epi.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/magasin.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
</html>