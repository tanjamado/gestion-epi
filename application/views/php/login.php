<!DOCTYPE html>
<html lang="fr">
<head>

    <title>Gestion EPI - Connexion</title>

     <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/login.css">

    <?php include("head.php"); ?>
</head>

<body>
<div class="login-form" id="connectForm">
    <div class="header animated zoomIn" align="center">

        <h1>
            <div class="bold">Bienvenue sur gestion-epi.org</div>
        </h1>
        <span>Entrez votre identifiant et mot de passe</span>

    </div>

    <div class="content">
        <input name="username" type="text" class="input username animated zoomIn"
               placeholder="Identifiant" id="username" autocomplete="" onkeypress="textChanged(event)"/>
        <div class="user-icon"></div>
        <input name="password" type="password" class="input password animated zoomIn" placeholder="Mot de passe"
               id="password" autocomplete="" onkeypress="runConnect(event)"/>
        <div class="pass-icon"></div>
        <p class="success"></p>
    </div>

    <div class="footer">
        <div class="row flex-center">
            <div class="col-12">

                <button class="btn special-color btn-block text-white my-4 animated zoomIn" id="connexionButton">
                    Connexion
                </button>


                <!--<a class="button btn-info animated zoomIn" id="connexionButton" onclick="connexion()">
                    Connexion
                </a>-->
            </div>
            <div class="col-12">
                <a class="forgotten_password animated zoomIn" id="modalActivate" data-toggle="modal"
                   data-target="#exampleModalPreview">
                    Mot de passe oublié
                </a>

                <div class="modal fade fadeInDown" id="exampleModalPreview" tabindex="-1"
                     aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="header animated zoomIn" align="center">

                                <h1>
                                    <div class="bold">Récupération de votre mot de passe</div>
                                </h1>

                            </div>
                            <div class="content">
                                <input name="certificat" type="text" class="input animated zoomIn mb-4"
                                       placeholder="Saisissez votre identifiant" id="certificat"
                                       onkeypress="runPassword(event)"/>
                                <input name="email" type="email" class="input animated zoomIn"
                                       placeholder="Saisissez votre adresse email" id="email"
                                       onkeypress="runPassword(event)"/>
                                <p class="passwordForgotten"></p>
                            </div>
                            <div class="contentText animated zoomIn">
                                En cliquant sur "Récupérer" :
                                <div class="list-group list-group-flush text-left">
                                    <p class="list-group-item bg-transparent border-0">Un email vous sera envoyé afin de
                                        vous transmettre un mot de passe provisoire et vous notifiera qu'il faudra le
                                        changer rapidement
                                    </p>
                                </div>
                            </div>
                            <div class="footer">

                                <button class="btn special-color btn-block text-white my-4 animated zoomIn"
                                        id="getPassword" type="submit">
                                    Récupérer
                                </button>


                                <!--<a class="button btn-info" onclick="passwordForgotten()">Récupérer</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</body>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/connexion.js" type="text/javascript"></script>

</html>