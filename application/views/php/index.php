<!DOCTYPE html>
<html lang="fr">
<head>

    <title>Gestion EPI - Accueil</title>

    <?php include("head.php"); ?>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/accueil.css">

</head>

<?php include("nav.php"); ?>

<body>
<div class="container mt-5">
    <div class="row">
        <div class="col-12">
            <div class="circle animated fadeIn">
                <div class="anim">
                    <span>Bienvenue</span><br/>
                </div>
                <div class="anim-text">
                    <span>Gérez vos EPI</span><br/>
                    <span>en toute simplicité !</span>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/modules/default-file-input.js"
        type="text/javascript"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript"
        src="https://cdn.datatables.net/v/bs4/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/datatables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
</html>