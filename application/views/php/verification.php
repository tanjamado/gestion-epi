<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Gestion EPI - Vérification d'un EPI</title>

    <?php header('Access-Control-Allow-Origin: *'); ?>
    <?php include("head.php"); ?>

</head>

<?php include("nav.php"); ?>

<body>
<div class="container bg-light pt-5 animated fadeIn mt-5 mb-5">
    <div class="row pb-2">
        <div class="col-12">
            <h3 class="panel-title">Vérification d'un EPI</h3>
        </div>
    </div>

    <div class="container p-4" id="containerPDF">
        <div class="row mr-1 ml-1">
            <div class="col-12 col-md-6">
                <div class="row">
                    <label for="groupe">Groupe *</label>
                    <select class="browser-default custom-select mb-3" id="groupe">
                    </select>
                </div>
                <div class="row">
                    <label for="lot">Lot *</label>
                    <select class="browser-default custom-select mb-3" id="lot">
                    </select>
                </div>
                <div class="row">
                    <label for="type">Type d'EPI *</label>
                    <select class="browser-default custom-select mb-3" id="type">
                    </select>
                </div>
                <div class="row">
                    <label for="epi">Identification individuelle *</label>
                    <select class="browser-default custom-select mb-3" id="epi">
                    </select>
                </div>
            </div>

            <div class="col-12 col-md-6 mb-md-4 mt-md-4">
                <div class="row mt-2">
                    <div class="col-12 col-md-12">
                        <div class="row mb-3 mt-2 ml-md-1">
                            <label>Date de mise en service : </label>
                            <p id="date-mise-en-service" class="ml-1"></p>
                        </div>
                        <div class="row mb-3 ml-md-1">
                            <label>Date de la dernière verification : </label>
                            <p id="date-derniere-verif" class="ml-1"></p>
                        </div>
                        <div class="row mb-3 ml-md-1">
                            <label for="lien-pdf">Lien vers le précédent pdf : </label>
                            <a id="lien-pdf" target="_blank" class="ml-1"></a>
                        </div>
                        <div class="row mb-3 ml-md-1">
                            <label>Date de la vérification actuel : </label>
                            <p id="date-verif-actuel" class="ml-1"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <form id="verification_form">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="" id="cardInformation">
                            <p id="information"></p>
                        </div>
                        <div class="card-body" id="mainRow">
                            <div class="row mt-1 mb-4">
                                <div class="col-12" id="cardVisuelleVisible">
                                    <div class="card">
                                        <h4 class="mt-4 ml-3 mb-0 pb-0">Vérifications visuelles</h4>

                                        <div class="card-body" id="cardVisuel">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4 mb-4">
                                <div class="col-12" id="cardFonctionnelVisible">
                                    <div class="card">
                                        <h4 class="mt-4 ml-3 mb-0 pb-0">Vérifications fonctionnelles</h4>

                                        <div class="card-body" id="cardFonctionnel">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 " align="center">
                                    <div class="radio">
                                        <input type="radio" class="radio-input" name="etatRadio" id="apteRadio"
                                               value="Apte">
                                        <label class="radio-label" for="apteRadio"><span
                                                    style="font-size: 140%">Apte</span></label>

                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 " align="center">
                                    <div class="radio">
                                        <input type="radio" class="radio-input" name="etatRadio" id="quarantaineRadio"
                                               value="Quarantaine">
                                        <label class="radio-label" for="quarantaineRadio"><span
                                                    style="font-size: 140%">Quarantaine</span></label>
                                    </div>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 " align="center">
                                    <div class="radio">
                                        <input type="radio" class="radio-input" name="etatRadio" id="rebutRadio"
                                               value="Rebut">
                                        <label class="radio-label" for="rebutRadio"><span
                                                    style="font-size: 140%">Rebut</span></label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <label for="commentaires">Commentaires</label>
                                    <textarea type="text" id="commentaires" class="form-control mb-4"
                                              style="resize: none" maxlength="400"></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <p id="success"></p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-12">
                    <div class="col-xs-12 mb-n4 col-sm-12 col-md-3 col-lg-3 float-right">
                        <button type="submit" class="btn special-color btn-block text-white my-4" id="submitButton">
                            Valider
                        </button>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
</body>

<!--Modal: modalConfirmModify-->
<div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-sm modal-notify modal-info" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">Télécharger le PDF de la vérification réalisée ?</p>
            </div>

            <!--Body-->
            <div class="modal-body">

                <i class="far fa-file-pdf fa-4x animated bounceIn"></i>

            </div>

            <!--Footer-->
            <div class="modal-footer flex-center">
                <button class="btn btn-outline-info" id="pdfDownload">Oui</button>
                <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Non</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: modalConfirmModify-->

<!--Modal: modalShowOldPicture-->
<div class="modal fade" id="modalPicture" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-md modal-notify modal-info" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">Photo de la vérification précédente</p>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">

                <img id="old-picture" alt="image" src=""/>

            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: modalShowOldPicture-->

<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/jquery-3.3.1.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/bootstrap.min.js"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/lib/material-design-4.7.1/js/mdb.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/leave_app.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/verification_epi.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"
        type="text/javascript"></script>
</html>