<?php
/**
 * Created by PhpStorm.
 * User: Emeric PAIN
 * Date: 20/03/2019
 * Time: 09:20
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("Select type_epi, numero_serie, marque, date_fin_de_vie, 
       (if (exists(select date_verification from verifications where epi = numero_serie order by date_verification desc limit 1), (select date_verification from verifications where epi = numero_serie order by date_verification desc limit 1), 'Non vérifié')) as date_verification, 
       (if (exists(select etat from verifications where epi = numero_serie order by date_verification desc limit 1), (select etat from verifications where epi = numero_serie order by date_verification desc limit 1), 'Non vérifié')) as date_verification from epi where lot = ? and (if (exists(select etat from verifications where epi = numero_serie order by date_verification desc limit 1), (select etat from verifications where epi = numero_serie order by date_verification desc limit 1), 'Non vérifié')) != 'Rebut'");
$query->execute(array($_POST['lot']));
$epi = array();

$epi = $query->fetchAll();

ob_get_clean(); //pour clean echo
echo json_encode($epi);