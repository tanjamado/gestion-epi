<?php

include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();
$query = $bdd->prepare("SELECT * FROM verificateurs WHERE certificat = ? AND mot_de_passe = ?");
$query->execute(array($_POST['username'], hash("sha256", $_POST['password'])));

$userExists = $query->rowCount();

ob_get_clean();
if ($userExists == 1)
{
    session_start();
    $userInfo = $query->fetch();
    $_SESSION['certificat'] = $userInfo['certificat'];
    //$_SESSION['password'] = $userInfo['mot_de_passe'];
    $_SESSION['nom'] = $userInfo['nom'];
    $_SESSION['prenom'] = $userInfo['prenom'];
    $_SESSION['mail'] = $userInfo['mail'];
    $_SESSION['permission'] = $userInfo['permission'];
    echo json_encode($_SESSION);
}
else
{
    $_SESSION['failed'] = "Failed";
    echo json_encode($_SESSION);
}
