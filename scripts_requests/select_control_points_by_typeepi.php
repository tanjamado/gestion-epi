<?php
/**
 * Created by PhpStorm.
 * User: Emeric PAIN
 * Date: 15/04/2019
 * Time: 12:33
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("Select id_point_de_controle, libelle, type_verification from point_de_controle where type_epi = ?");
$query->execute(array($_POST['type_epi']));
$pointControle = $query->fetchAll();

ob_get_clean(); //pour clean echo
echo json_encode($pointControle);