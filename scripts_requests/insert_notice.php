<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 30/04/2019
 * Time: 14:46
 */

include("../application/controllers/Connexion.php");
include("../application/controllers/FtpController.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();
$result = array();

try {
    $query = $bdd->prepare("SELECT nom FROM marque WHERE nom = ?");
    $query->execute(array($_POST['marque']));

    $query->closeCursor();

    if ($query->rowCount() == 0) {
        $query = $bdd->prepare("INSERT INTO marque (nom) values (?)");
        $result['success'] = $query->execute(array($_POST['marque']));

        $query->closeCursor();

        if (!$result["success"]) {
            ob_get_clean();
            echo json_encode($result);
            exit();
        }
    }

    $query = $bdd->prepare("SELECT message_notice FROM notice WHERE marque = ? AND type_epi = ? and modele = ?");
    $query->execute(array($_POST["marque"], $_POST['type_epi'], $_POST['modele']));

    if (strcmp("true", $_POST['isFile']) == 0) {

        try {

            $filename = "/pdf_notices/" . $_POST['marque'] . "_" . $_POST['type_epi'] . "_" . $_POST['modele'] . "." . pathinfo($_FILES['message_notice']['name'])['extension'];
            if (file_exists(dirname(__DIR__, 1) . $filename))
                unlink(dirname(__DIR__, 1) . $filename);
            $moved = move_uploaded_file($_FILES['message_notice']['tmp_name'], dirname(__DIR__, 1) . $filename);

            if ($query->rowCount() == 0) {

                $query = $bdd->prepare("INSERT INTO notice (message_notice, type_epi, marque, modele) VALUES (:message_notice, :type_epi, :marque, :modele)");

                $result['success'] = $query->execute(array(
                    'message_notice' => $_SERVER['SERVER_NAME'] . $filename,
                    'type_epi' => $_POST['type_epi'],
                    'marque' => $_POST['marque'],
                    'modele' => $_POST['modele']
                ));
                $query->closeCursor();

            } else {

                $query = $bdd->prepare("UPDATE notice SET message_notice = ? WHERE type_epi = ? and marque = ? and modele = ?");

                $result['success'] = $query->execute(array(
                    $_SERVER['SERVER_NAME'] . $filename,
                    $_POST['type_epi'],
                    $_POST['marque'],
                    $_POST['modele']
                ));
                $query->closeCursor();
            }
        } catch (Exception $exception) {
            ob_get_clean();
            $result['success'] = $exception->getMessage();
            echo json_encode($result);
            exit();
        }
    } else {
        if ($query->rowCount() == 0) {

            $query = $bdd->prepare("INSERT INTO notice (message_notice, type_epi, marque, modele) VALUES (:message_notice, :type_epi, :marque, :modele)");

            $result['success'] = $query->execute(array(
                'message_notice' => $_POST['message_notice'],
                'type_epi' => $_POST['type_epi'],
                'marque' => $_POST['marque'],
                'modele' => $_POST['modele']
            ));
            $query->closeCursor();

        } else {

            $query = $bdd->prepare("UPDATE notice SET message_notice = ? WHERE type_epi = ? and marque = ? and modele = ?");

            $result['success'] = $query->execute(array(
                $_POST['message_notice'],
                $_POST['type_epi'],
                $_POST['marque'],
                $_POST['modele']
            ));
            $query->closeCursor();
        }
    }

    ob_get_clean();
    echo json_encode($result);

} catch (PDOException $exception) {
    ob_get_clean();
    $result['success'] = $exception->getMessage();
    echo json_encode($result);
}