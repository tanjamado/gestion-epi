<?php
/**
 * Created by PhpStorm.
 * User: andre
 * Date: 22/05/2019
 * Time: 10:58
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("SELECT image FROM controles WHERE epi_verification = ? AND point_de_controle = ? order by date_verification desc limit 1");
$query->execute(array(
    $_POST['epi'],
    $_POST['control_point']
));

$result = $query->fetchAll();

ob_get_clean();
echo json_encode($result);