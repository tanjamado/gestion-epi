<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 23/04/2019
 * Time: 16:10
 */

session_start();
ob_get_clean();

echo json_encode($_SESSION);