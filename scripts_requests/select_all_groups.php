<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 17/04/2019
 * Time: 17:59
 */

include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

ob_get_clean(); //pour clean echo
$sql = "SELECT nom_groupe FROM groupe";
$query = $bdd->prepare($sql);
$query->execute();
$list_groupe = array();
$i = 0;
$list_groupe = $query->fetchAll();
echo json_encode($list_groupe);