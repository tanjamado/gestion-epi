<?php
/**
 * Created by PhpStorm.
 * User: Emeric PAIN
 * Date: 03/04/2019
 * Time: 11:52
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$result = array();

session_start();
ob_get_clean();

try {

    $filename = "/pdf_verifications/" . str_replace(" ", "_", $_POST['groupe']) . "/" . str_replace(" ", "_", $_POST['lot']) . "/" . "Vérification_" . $_POST['date'] . "_EPI_" . $_POST['epi'] . ".pdf";

    $queryVerification = $bdd->prepare("INSERT INTO verifications (date_verification, observations, epi, etat, verificateur, url_pdf) values (?, ?, ?, ?, ?, ?)");
    $result['success'] = $queryVerification->execute(array(
        $_POST['date'],
        $_POST['commentaires'],
        $_POST['epi'],
        $_POST['etat'],
        $_SESSION['certificat'],
        $_SERVER['SERVER_NAME'] . $filename
    ));
    $queryVerification->closeCursor();

    if ($result['success'] == FALSE) {
        echo json_encode($result);
        exit();
    } else {
        if (file_exists(dirname(__DIR__, 1) . $filename)) {
            unlink(dirname(__DIR__, 1) . $filename);
        }
        $moved = move_uploaded_file($_FILES['pdf']['tmp_name'], dirname(__DIR__,1) . $filename);
    }

    $controles = json_decode($_POST['controles'], true);

    foreach ($controles as $controle) {
        if (isset($controle['image'])) {
            $query = $bdd->prepare("INSERT INTO controles (etat, image, observations, point_de_controle, date_verification, epi_verification) VALUES (:etat, :image, :observations, :point_de_controle, :date_verification, :epi_verification)");
            $result['success'] = $query->execute(array(
                'etat' => $controle['etat'],
                'image' => $controle['image'],
                'observations' => $controle['observations'],
                'point_de_controle' => $controle['point_de_controle'],
                'date_verification' => $_POST['date'],
                'epi_verification' => $_POST['epi']
            ));
            $query->closeCursor();
        } else {
            $file = $_FILES['image' . $controle['point_de_controle']];
            $filename = "/images_epi/" . $controle['point_de_controle'] . "_" . $_POST['date'] . "_" . $_POST['epi'] . "." . pathinfo($file['name'])['extension'];

            $query = $bdd->prepare("INSERT INTO controles (etat, image, observations, point_de_controle, date_verification, epi_verification) VALUES (:etat, :image, :observations, :point_de_controle, :date_verification, :epi_verification)");
            $result['success'] = $query->execute(array(
                'etat' => $controle['etat'],
                'image' => $_SERVER['SERVER_NAME'] . $filename,
                'observations' => $controle['observations'],
                'point_de_controle' => $controle['point_de_controle'],
                'date_verification' => $_POST['date'],
                'epi_verification' => $_POST['epi']
            ));
            $query->closeCursor();

            if ($result['success'] == TRUE) {
                if (file_exists(dirname(__DIR__, 1) . $filename)) {
                    unlink(dirname(__DIR__, 1) . $filename);
                }
                move_uploaded_file($file['tmp_name'], dirname(__DIR__, 1) . $filename);
            }
        }

        if ($result['success'] == FALSE) {
            echo json_encode($result);
            exit();
        }
    }

    echo json_encode($result);

} catch (PDOException $exception) {
    $result['success'] = $exception->getMessage();
    echo json_encode($result);
    exit();
}