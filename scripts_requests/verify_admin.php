<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 11/05/2019
 * Time: 15:33
 */

if ($_SESSION['permission'] != 0) {
    header("location:/profil");
    exit();
}