<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 07/05/2019
 * Time: 21:50
 */

include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("SELECT nom, prenom, certificat FROM verificateurs");
$query->execute();
$result = $query->fetchAll();

ob_get_clean();
echo json_encode($result);