<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 11/05/2019
 * Time: 15:53
 */

include("../application/controllers/Connexion.php");
$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

session_start();
$result = array();

if ($_SESSION["permission"] != 0) {
    ob_get_clean();
    $result["success"] = "Vous n'avez pas les droits nécessaires";
    echo json_encode($result);
    exit();
}

try {
    if ($_SESSION['certificat'] == $_POST['certificat']) {
        $result["success"] = "Administrateur";
        echo json_encode($result);
    } else {
        $query = $bdd->prepare("DELETE FROM verificateurs WHERE certificat = ?");
        $result["success"] = $query->execute(array($_POST['certificat']));

        ob_get_clean();
        echo json_encode($result);
    }
} catch (Exception $exception) {
    ob_get_clean();
    $result["success"] = $exception->getMessage();
    echo json_encode($result);
}
