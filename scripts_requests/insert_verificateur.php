<?php
/**
 * Created by PhpStorm.
 * User: Emeric PAIN
 * Date: 03/04/2019
 * Time: 11:15
 */
include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();
ob_get_clean();
session_start();
$query = $bdd->prepare("INSERT INTO verificateurs (certificat, nom, prenom, mot_de_passe, mail, permission) VALUES ( ?, ?, ?, ?, ?, 1)");
$success = array();

try {
    if ($_SESSION['permission'] != 0) {
        $success['success'] = false;
        echo json_encode($success);
        exit();
    }
    $success['success'] = $query->execute(array($_POST['certificat'], $_POST['nom'], $_POST['prenom'], hash("sha256", $_POST['password']), $_POST['mail']));
    echo json_encode($success);
} catch (Exception $e) {
    $success['success'] = "Duplicata";
    echo json_encode($success);
}
