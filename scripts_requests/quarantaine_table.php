<?php
/**
 * Created by PhpStorm.
 * User: Kilian
 * Date: 07/05/2019
 * Time: 14:57
 */

include("../application/controllers/Connexion.php");

$bdd = \controler\connexion\Connexion::getInstance()->getBdd();

$query = $bdd->prepare("Select type_epi, numero_serie,
       (if (exists(select etat from verifications where epi = numero_serie order by date_verification desc limit 1), (select etat from verifications where epi = numero_serie order by date_verification desc limit 1), 'Non vérifié')) as etat,
       marque, lot, date_fin_de_vie from epi where (if (exists(select etat from verifications where epi = numero_serie order by date_verification desc limit 1), (select etat from verifications where epi = numero_serie order by date_verification desc limit 1), 'Non vérifié')) = 'Quarantaine'");

$result = $query->execute();
$synthese = $query->fetchAll();

ob_get_clean();
echo json_encode($synthese);
