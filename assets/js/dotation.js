$(document).ready(function () {
    let url = new URL(window.location.href);
    let lot = url.searchParams.get("nom_lot");
    let groupName = url.searchParams.get("nom_groupe");

    /**
     * Création de la table des epi
     */
    let promise = new Promise(function (resolve) {
        resolve(loadDatatable(lot, groupName));
    });

    promise.then(
        function () {
            setTimeout(function () {
                if ($("#table-epi tbody").find("td:first").text() === "Aucun EPI à afficher")
                    $("#modalButton").attr("disabled", "");
                else
                    $("#modalButton").removeAttr("disabled");
            }, 300);
        }
    );

    /**
     * Récupération de la liste des groupes/structures
     */
    $.get(
        '/scripts_requests/select_all_groups_without_stock.php',
        function (data) {
            let structures = JSON.parse(data);
            for (let i = 0; i < structures.length; i++) {
                let structure = structures[i]['nom_groupe'];
                if (structure === groupName) {
                    $("#structure").append('<option selected="" value="' + structure +
                        '">' + structure +
                        '</option>')
                } else {
                    $("#structure").append('<option value="' + structure +
                        '">' + structure +
                        '</option>')
                }
            }
        }
    ).then(function () {
        /**
         * Récupération des lots correspondants au premier groupe
         */
        $.post(
            '/scripts_requests/select_lots_by_group.php',
            {
                groupe: $("#structure").val(),
            },
            function (data) {
                let lots = JSON.parse(data);
                let exists = false;
                let lotBox = $("#lot");
                for (let i = 0; i < lots.length; i++) {
                    let currentLot = lots[i]['nom_lot'];
                    if (lot !== currentLot) {
                        lotBox.append('<option value="' + currentLot +
                            '">' + currentLot +
                            '</option>');
                    } else {
                        exists = true;
                        lotBox.append('<option selected="" value="' + currentLot +
                            '">' + currentLot +
                            '</option>');
                    }
                }
                if (!exists) {
                    lotBox.append("<option hidden selected='' disabled='' value='" + "unValide" +
                        "'>" + "Le lot n'existe pas" + "</option>");
                } else if (lots.length === 0)
                    lotBox.append("<option hidden disabled='' selected=''>Aucun lot enregistré</option>");

                lot = lotBox.val();
                groupName = $("#structure").val();
            },
            'text'
        );
    });

    /**
     * Lorsque l'on change de lot on détruit la table et réactualise le tableau
     */
    $("#lot").on('change', function () {
        lot = $("#lot").val();
        groupName = $("#structure").val();

        loadEpiByLotAndGroup(lot, groupName);

        let promise = new Promise(function (resolve) {
            resolve(loadDatatable(lot, groupName));
        });

        promise.then(
            function () {
                setTimeout(function () {
                    if ($("#table-epi tbody").find("td:first").text() === "Aucun EPI à afficher")
                        $("#modalButton").attr("disabled", "");
                    else
                        $("#modalButton").removeAttr("disabled");
                }, 300);
            }
        );
    });

    /**
     * On charge les lots du groupe lorsque celui-ci change
     */
    $("#structure").on("change", function () {
        let group = $("#structure").val();
        loadLotByGroup(lot, group);
    });

    /**
     * Lorsque l'on clique sur une ligne cela renvoie vers la vérification de l'EPI en question
     */
    $('#table-epi tbody').on('click', 'tr', function () {
        let numero_serie = $(this).find('td').eq(1).text();
        let epiType = $(this).find('td').eq(0).text();
        if (epiType !== "Aucun EPI à afficher")
            verification(numero_serie, epiType, lot, groupName);
    });

    loadEpiByLotAndGroup(lot, groupName);

    $("#submitButton").on("click", function () {
        putEpiStock(lot, groupName);
    })
});

/**
 * Fonction initialisant la table avec le lot et le groupe passé en paramètre
 * @param lot
 * @param groupName
 */
function loadDatatable(lot, groupName) {
    $("#table-epi").DataTable({
        "destroy": true,
        "ajax": {
            "url": "/scripts_requests/dotation_table.php",
            "dataSrc": "",
            "data": {
                "lot": lot,
                "nom_groupe": groupName,
            },
            "type": 'POST',
        },
        "dom": "Bftipr",
        "pagingType": "simple",
        "searching": true,
        "language": {
            "emptyTable": "Aucun EPI à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ EPI affichés",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table des EPI ...",
            "processing": "Recherche des EPI ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ EPI)",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas d'EPI correspondants à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        buttons: {
            dom: {
                button: {
                    tag: 'button',
                    className: ''
                }
            },
            buttons: [{
                extend: 'pdf',
                className: 'btn special-color text-white mr-2',
                text: 'Tableau de dotations PDF',
                extension: '.pdf',
                action: function (e, dt, node, config) {
                    exportTableToPDF(e, dt, node, config, $("#table-epi").dataTable().fnGetData(), lot);
                }
            }]
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });
}

/**
 * Fonction chargeant la liste des EPI dans une box à l'aide d'un lot et d'un groupe
 * @param lot
 * @param groupName
 */
function loadEpiByLotAndGroup(lot, groupName) {
    $.post(
        '/scripts_requests/select_epis_by_lot.php',
        {
            lot: lot,
        },
        function (data) {
            let epiBox = $("#epi");

            let epis = JSON.parse(data);
            epiBox.empty();
            epiBox.append('<option hidden value="">Choisir l\'EPI...</option>');
            for (let i = 0; i < epis.length; i++) {
                epiBox.append('<option value="' + epis[i]['numero_serie'] +
                    '">' + epis[i]['numero_serie'] +
                    '</option>');
            }
        },
        'text'
    );
}

/**
 * Fonction de chargement des lots d'un groupe
 */
function loadLotByGroup(lot, groupName) {
    $.post(
        '/scripts_requests/select_lots_by_group.php',
        {
            groupe: groupName,
        },
        function (data) {
            let lots = JSON.parse(data);
            let lotBox = $("#lot");
            lotBox.empty();
            lotBox.append("<option hidden value='' disabled='' selected=''>Choisissez un lot</option>");
            for (let i = 0; i < lots.length; i++) {
                let lot = lots[i]['nom_lot'];
                lotBox.append('<option value="' + lot +
                    '">' + lot +
                    '</option>');
            }
            if (lots.length === 0)
                lotBox.append("<option hidden disabled='' selected=''>Aucun lot enregistré</option>");
        },
        'text'
    );
}

/**
 * Fonction qui renvoie vers la vérification d'un EPI passé en paramètre
 * @param numero_serie
 * @param type_epi
 * @param nom_lot
 * @param groupe
 */
function verification(numero_serie, type_epi, nom_lot, groupe) {
    window.location.href = "/verification?numero_serie=" + numero_serie + "&type_epi=" + type_epi + "&nom_lot=" + nom_lot + "&groupe=" + groupe;
}

/**
 * Fonction d'exportation du tableau des EPI dans un fichier PDF
 * @param e
 * @param dt
 * @param node
 * @param config
 * @param data Les données de la table
 * @param lot Le lot correspondant
 */
function exportTableToPDF(e, dt, node, config, data, lot) {
    let exportDocument = new jsPDF("p", "pt");

    console.log(data);

    let currentPage = 1;

    let header = function () {
        exportDocument.setFontSize(18);
        exportDocument.setTextColor(40);
        exportDocument.setFontStyle('normal');
        let texte = exportDocument.splitTextToSize("Tableau de dotations des équipements de protection individuelle contre les chutes de hauteur", exportDocument.internal.pageSize.width - 40);
        exportDocument.text(texte, exportDocument.internal.pageSize.width / 2, 50, null, null, 'center');
        exportDocument.setFontSize(12);
        texte = exportDocument.splitTextToSize("CFA/CFPPA Tours Fondettes Agrocampus", exportDocument.internal.pageSize.width - 40);
        exportDocument.text(texte, 35, 110, null, null, 'left');
        exportDocument.text("Lot : " + lot, 35, 130, null, null, 'left');
        exportDocument.text("Nom et prénom de l'utilisateur : ", 35, 150, null, null, 'left');
    };

    let columns = [
        {
            title: "Type EPI",
        },
        {
            title: "Identification individuelle",
        },
        {
            title: "Marque",
        },
        {
            title: "Fin de vie",
        },
        {
            title: "Dernière vérification",
        },
        {
            title: "État",
        },
    ];

    exportDocument.autoTable(
        columns,
        data,
        {
            didDrawPage: header,
            margin: {
                top: 165,
                left: 35,
                right: 35,
                bottom: 80,
                width: 560,
            },
        }
    );


    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;

    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    today = dd + '/' + mm + '/' + yyyy;

    if (exportDocument.autoTable.previous.finalY > exportDocument.internal.pageSize.height - 50) {
        let nbPages = exportDocument.internal.getNumberOfPages() + 1;
        exportDocument.text(currentPage + "/" + nbPages, exportDocument.internal.pageSize.width - 20, exportDocument.internal.pageSize.height - 20, null, null, 'right');
        exportDocument.addPage();
        currentPage++;
    }

    let texte = "Cette dotation en Equipements de Protection Individuelle (EPI) vous est fournie personnellement, sur la base de l’analyse des risques auxquels vous êtes exposés.\n\n" +
        "Conformément à l’article L 4122-1 du code du travail, il vous appartient de porter ces équipements et de signaler toute défectuosité ou usure prématurée afin de procéder à son éventuelle réparation et remplacement.\n\n\n" +
        "Date d'édition de la fiche de dotation : " + today + "\n\n\n" +
        "Je reconnais avoir reçu ma dotation ainsi que les informations relatives à l’utilisation, l’entretien et à l’obligation de port de ces équipements.\n\n\n\n" +
        "Signature du salarié :\t\t\t\t\t\t\t\t\t\tSignature du vérificateur :\n";

    texte = exportDocument.splitTextToSize(texte, exportDocument.internal.pageSize.width + 90);
    exportDocument.setFontSize(12);
    exportDocument.text(texte, 35, exportDocument.autoTable.previous.finalY + 30, null, null, "left");

    exportDocument.text(currentPage + "/" + exportDocument.internal.getNumberOfPages(), exportDocument.internal.pageSize.width - 20, exportDocument.internal.pageSize.height - 20, null, null, 'right');

    exportDocument.save("Tableau_Dotations_Lot_" + lot + ".pdf");
}

/**
 * Fonction réalisant le changement d'un EPI vers le stock
 */
function putEpiStock(lot, groupName) {
    event.preventDefault();

    let epi = $("#epi").val();

    console.log(epi);

    if (epi === "") {
        $.getScript("assets/js/utils.js", function () {
            customErrorMessage("#errorMessage","Veuillez saisir l'EPI à déplacer dans le stock");
        });
        return false;
    }

    $.post(
        '/scripts_requests/update_epi_set_lot.php',
        {
            numero_serie: epi,
            nom_lot: "Stock",
        },
        function (data) {
            try {
                let success = JSON.parse(data);

                if (success['success'] === true) {
                    $.getScript("assets/js/utils.js", function () {
                        displayTemporaryInfoMessage("Affectation de l'EPI au stock réussie");
                    });

                    $("#exampleModalPreview").modal("hide");

                    loadDatatable(lot, groupName);
                } else {
                    $.getScript("assets/js/utils.js", function () {
                        displayErrorMessage("Impossible d'affecter l'EPI au stock");
                    });
                }
            } catch (e) {
                console.log(e);
                $.getScript("assets/js/utils.js", function () {
                    displayErrorMessage("Erreur lors de l'affection de l'EPI au stock");
                })
            }
        },
        'text'
    )
}