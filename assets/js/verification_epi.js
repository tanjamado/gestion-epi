$(document).ready(function () {
    /**
     * On initialise l'action de création de la vérification
     */
    $("#submitButton").on("click", function () {
        createVerification();
    });

    /**
     * On charge les différents champs nécessaires
     */

    loadFields();

    $("#epi").on("change", function () {
        loadControlPoints();
        $("#commentaires").val("");
        $("input:checked[name=etatRadio]").prop("checked", false);
        $.getScript("assets/js/utils.js", function () {
            hideMessage();
        });

        loadInfosEpi();
    });

    let done = false;
    $("input[value=Apte]").on("click", function () {
        if (done === false) {
            done = true;
            let i = 0;
            $("form#verification_form .control-point").each(function () {
                $(this).find("input[id=bonBox" + i + "]").prop("checked", true);
                i++;
            });
        }
    });
});

/**
 * Fonction de création de la vérification dans la BD
 */
function createVerification() {
    event.preventDefault();

    let epi = $("#epi").val();
    let isApte = $("input:checked[name=etatRadio]").val();
    let comments = $("#commentaires").val();
    let groupe = $("#groupe").val();
    let lot = $("#lot").val();

    let controles = {};

    let i = 0;
    let complete = true;

    let form = new FormData();

    /**
     * On récupère les champs sélectionnés de la vérification
     */
    $("form#verification_form .control-point").each(function () {
        //let controlName = $(this).find(".control-name").text().trim();
        let controlId = $(this).find("input:checked").attr("name");
        let etat = $(this).find("input:checked").val();
        let image = $(this).find("#file-with-current" + i).prop("files")[0];

        /**
         * On vérifie qu'un état est bien sélectionné
         */
        if (etat === undefined) {
            $.getScript("assets/js/utils.js", function () {
                displayErrorMessage("L'un des points de contrôle n'a pas été vérifié");
            });
            complete = false;
        }

        controles[i] = {};
        /**
         * Si il y a une image on set le $_FILES sinon le $_POST
         */
        if (image === undefined) {
            image = "";
            controles[i]['image'] = image;
        } else {
            form.append("image" + controlId, image);
        }

        controles[i]['point_de_controle'] = controlId;
        controles[i]['etat'] = etat;
        controles[i]['observations'] = "";
        i++;
    });

    console.log(controles);
    console.log(isApte);

    if (isApte === undefined) {
        $.getScript("assets/js/utils.js", function () {
            displayErrorMessage("Vous n'avez pas saisi l'état final de la vérification");
        });
        return false;
    }

    if (!complete)
        return false;

    /**
     * On récupère la date du jour au format mm-yyyy
     * @type {Date}
     */
    let today = new Date();
    let mm = today.getMonth() + 1;

    let yyyy = today.getFullYear();
    if (mm < 10) {
        mm = '0' + mm;
    }
    let date = mm + '-' + yyyy;

    let pdf = exportPDF(false);

    /**
     * On ajoute au FormData les données nécessaires
     */
    form.append("epi", epi);
    form.append("etat", isApte);
    form.append("date", date);
    form.append("commentaires", comments);
    form.append("pdf", pdf);
    form.append("controles", JSON.stringify(controles));
    form.append("lot", lot);
    form.append("groupe", groupe);

    /**
     * On appelle l'enregistrement de la vérification
     */
    $.blockUI({
        theme: false,
        baseZ: 2000,
        css: {
            border: 'none',
            padding: '15px',
            backgroundColor: '#000',
            '-webkit-border-radius': '10px',
            '-moz-border-radius': '10px',
            opacity: .5,
            color: '#fff'
        },
        message: '<h4><img alt="" src="../../assets/images/busy.gif" /><br><br> Création de la vérification en cours ...</h4>'
    });
    $.ajax({
        url: '/scripts_requests/insert_verification.php',
        data: form,
        cache: false,
        contentType: false,
        processData: false,
        type: "POST",
        method: "POST",
        success: function (data, textStatus, jqXHR) {
            try {
                console.log(data);
                let result = JSON.parse(data);
                if (result['success'] === true) {
                    $.getScript("assets/js/utils.js", function () {
                        displayTemporaryInfoMessage("Vérification réussie");
                    });

                    /**
                     * On propose à l'utilisateur d'enregistrer le PDF
                     */
                    $("#modalPDF").modal("show");
                    $("#pdfDownload").prop("enabled", false);

                    $('#modalPDF').on('hide.bs.modal', function () {
                        console.log("Modal hiding");

                        /**
                         * On remet les champs à zéro
                         */
                        loadControlPoints();
                        $("#commentaires").val("");
                        $("input:checked[name=etatRadio]").prop("checked", false);

                        loadEpiByGroupAndType();
                        loadInfosEpi();
                    });

                    $("#pdfDownload").on("click", function () {
                        let promise = new Promise(function (resolve) {
                            resolve(exportPDF(true));
                        });

                        promise.then(function () {
                            $("#pdfDownload").prop("enabled", true);
                            $("#modalPDF").modal("hide");
                        });
                    });

                } else if (result['success'].includes("Duplicata")) {
                    $.getScript("assets/js/utils.js", function () {
                        displayErrorMessage("Vous avez déjà réalisé une vérification sur cet EPI ce mois-ci");
                    });
                } else {
                    $.getScript("assets/js/utils.js", function () {
                        displayErrorMessage("L'insertion de la vérification dans la base de données a échoué");
                    });
                }
            } catch (e) {
                $.getScript("assets/js/utils.js", function () {
                    displayErrorMessage("Insertion de la vérification impossible dans la base de données");
                });
                console.log(e);
            }
        }
    }).then(function () {
        $.unblockUI();
    });
}

/**
 * Fonction qui charge les points de controles
 */
function loadControlPoints() {
    let typeEpi = $("#type").val();

    /**
     * On récupère les points de contrôles pour le type d'EPI saisi
     */
    $.post(
        '/scripts_requests/select_control_points_by_typeepi.php',
        {
            type_epi: typeEpi
        },
        function (data) {
            let controles = JSON.parse(data);

            let cardVisuel = $("#cardVisuel");
            let cardFonctionnel = $("#cardFonctionnel");

            let hasVisuel = false;
            let hasFonctionnel = false;

            cardVisuel.empty();
            cardFonctionnel.empty();
            for (let i = 0; i < controles.length; i++) {
                let controlType = controles[i]['type_verification'];
                let controlName = controles[i]['libelle'];
                let id = controles[i]['id_point_de_controle'];

                /**
                 * Si le point de contrôle est visuel on ajoute une ligne dans la partie visuelle
                 */
                if (controlType === "Visuelle") {
                    if (hasVisuel === false) {
                        hasVisuel = true;
                        $("#cardVisuelleVisible").show();
                    }

                    cardVisuel.append(
                        '<div class="row mt-3 mb-3 control-point">\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">\n' +
                        '                                <div class="bold control-name">\n' + controlName +
                        '                                    \n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-1">\n' +
                        '                                <div class="radio">\n' +
                        '                                    <input type="radio" class="radio-input" controlName="' + controlName + '" name="' + id + '" id="bonBox' + i + '" value="Bon">\n' +
                        '                                    <label class="radio-label" for="bonBox' + i + '">Bon</label>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2">\n' +
                        '                                <div class="radio">\n' +
                        '                                    <input type="radio" class="radio-input" controlName="' + controlName + '" name="' + id + '" id="toFollowBox' + i + '" value="À surveiller">\n' +
                        '                                    <label class="radio-label" for="toFollowBox' + i + '">À surveiller</label>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2">\n' +
                        '                                <div class="radio">\n' +
                        '                                    <input type="radio" class="radio-input" controlName="' + controlName + '" name="' + id + '" id="errorBox' + i + '" value="Défaut majeur">\n' +
                        '                                    <label class="radio-label" for="errorBox' + i + '">Défaut majeur</label>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2">\n' +
                        '                                <div class="input-default-wrapper">\n' +
                        '                                    <input type="file" id="file-with-current' + i + '" class="input-default-js"\n' +
                        '                                           accept="image/jpeg, image/png">\n' +
                        '                                    <label class="label-for-default-js rounded mb-3 bg-white" for="file-with-current' + i + '">\n' +
                        '                                        <span class="span-choose-file" id="span' + i + '">Image</span>\n' +
                        '                                        <div class="float-right span-browse">Rechercher</div>\n' +
                        '                                    </label>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2 mt-n2 mt-md-n2 mt-lg-n2 mt-xl-n3">' +
                        '                                <input type="button" id="modalButton' + i + '" class="btn btn-sm special-color" value="Photo dernière vérification">' +
                        '                            </div>' +
                        '                        </div>'
                    );
                }
                /**
                 * Sinon, on ajoute une ligne dans la partie fonctionnelle
                 */
                else {
                    if (hasFonctionnel === false) {
                        hasFonctionnel = true;
                        $("#cardFonctionnelVisible").show();
                    }

                    cardFonctionnel.append(
                        '<div class="row mt-3 mb-3 control-point">\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">\n' +
                        '                                <div class="bold control-name">\n' + controlName +
                        '                                    \n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-1">\n' +
                        '                                <div class="radio">\n' +
                        '                                    <input type="radio" class="radio-input" controlName="' + controlName + '" name="' + id + '" id="bonBox' + i + '" value="Bon">\n' +
                        '                                    <label class="radio-label" for="bonBox' + i + '">Bon</label>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2">\n' +
                        '                                <div class="radio">\n' +
                        '                                    <input type="radio" class="radio-input" controlName="' + controlName + '" name="' + id + '" id="toFollowBox' + i + '" value="À surveiller">\n' +
                        '                                    <label class="radio-label" for="toFollowBox' + i + '">À surveiller</label>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2">\n' +
                        '                                <div class="radio">\n' +
                        '                                    <input type="radio" class="radio-input" controlName="' + controlName + '" name="' + id + '" id="errorBox' + i + '" value="Défaut majeur">\n' +
                        '                                    <label class="radio-label" for="errorBox' + i + '">Défaut majeur</label>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2">\n' +
                        '                                <div class="input-default-wrapper">\n' +
                        '                                    <input type="file" id="file-with-current' + i + '" class="input-default-js"\n' +
                        '                                           accept="image/jpeg, image/png">\n' +
                        '                                    <label class="label-for-default-js rounded mb-3 bg-white" for="file-with-current' + i + '">\n' +
                        '                                        <span class="span-choose-file" id="span' + i + '">Image</span>\n' +
                        '                                        <div class="float-right span-browse">Rechercher</div>\n' +
                        '                                    </label>\n' +
                        '                                </div>\n' +
                        '                            </div>' +
                        '                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-2 mt-n2 mt-md-n2 mt-lg-n2 mt-xl-n3">' +
                        '                                <input type="button" id="modalButton' + i + '" class="btn btn-sm special-color" value="Photo dernière vérification">' +
                        '                            </div>' +
                        '                        </div>'
                    )
                }

                /**
                 * On définie l'action lorsque l'utilisateur importe un fichier
                 */
                $("#file-with-current" + i).on("change", function (e) {
                    let label = $("#span" + i);

                    if ($(this).val() !== "") {
                        if ($("#file-with-current" + i).prop("files")[0].size > 5242880) {
                            $.getScript("assets/js/utils.js", function () {
                                displayTemporaryErrorMessage("La taille de l'image est trop importante, 5Mo maximum");
                            });
                            $(this).val("");
                            $(".span-choose-file").text("Image de l'EPI");
                        } else {
                            label.val($(this).val());
                            label.html(e.target.files[0].name);
                        }
                    } else {
                        $(this).val("");
                        label.val("");
                        label.html("Image de l'EPI");
                    }
                });

                /**
                 * On définie l'action pour afficher l'image de la précédente vérification pour ce point
                 */
                $("#modalButton" + i).on("click", function () {
                    let result;
                    $.post(
                        '/scripts_requests/select_image_by_numeroserie_controlpoint.php',
                        {
                            epi: $("#epi").val(),
                            control_point: id,
                        },
                        function (data) {
                            try {
                                result = JSON.parse(data);

                                if (result[0] === undefined || result[0]['image'] === '') {
                                    $("#old-picture").prop("src", "");
                                    $("#old-picture").prop("alt", "Pas de photo lors de la dernière vérification pour ce point");
                                    $('#modalPicture').modal("show");
                                } else {
                                    $("#old-picture").prop("src", location.protocol + "//" + result[0]['image']);

                                    let image = new Image();
                                    image.src = location.protocol + "//" + result[0]['image'];

                                    /**
                                     * On respecte le ratio de l'image pour éviter les déformations
                                     */
                                    image.onload = function () {
                                        let maxWidth = 450; // Max width for the image
                                        let maxHeight = 500;    // Max height for the image
                                        let ratio = 0;  // Used for aspect ratio
                                        let width = image.width;    // Current image width
                                        let height = image.height;  // Current image height

                                        if (width > maxWidth && width > height) {
                                            ratio = width / height;
                                            $("#old-picture").css("height", maxWidth / ratio);
                                            $("#old-picture").css("width", maxWidth); // Set new width
                                        } else if (height > maxHeight && height > width) {
                                            ratio = height / width;
                                            $("#old-picture").css("width", maxHeight / ratio);
                                            $("#old-picture").css("height", maxHeight);
                                        } else {
                                            $("#old-picture").css("width", maxWidth);
                                            $("#old-picture").css("height", maxHeight);
                                        }

                                        $('#modalPicture').modal("show");
                                    };
                                }
                            } catch (e) {
                                console.log(e);
                                $("#old-picture").prop("src", "");
                                $("#old-picture").prop("alt", "Pas de photo lors de la dernière vérification pour ce point");
                                $('#modalPicture').modal("show");
                            }
                        }
                    );
                });

                /**
                 * On définie l'action lorsque l'utilisateur clique sur une comboBox à surveiller
                 */
                $("#toFollowBox" + i).on("click", function () {
                    $("input[value=Apte]").prop("checked", false);
                });

                /**
                 * On définie l'action lorsque l'utilisateur clique sur une comboBox défaut majeur
                 */
                $("#errorBox" + i).on("click", function () {
                    $("input[value=Apte]").prop("checked", false);
                    $("input[value=Rebut]").prop("checked", true);
                });
            }

            /**
             * On masque la card visuelle s'il n'y a pas de vérifications visuelles
             */
            if (hasVisuel === false) {
                $("#cardVisuelleVisible").hide();
                cardVisuel.empty();
            }

            /**
             * On masque la card fonctionnelle s'il n'y a pas de vérifications fonctionnelles
             */
            if (hasFonctionnel === false) {
                $("#cardFonctionnelVisible").hide();
                cardFonctionnel.empty();
            }
        },
        'text'
    )
}

/**
 * Fonction de chargement des champs
 */
function loadFields() {
    let url = new URL(window.location);
    let groupe = url.searchParams.get("groupe");
    let lotName = url.searchParams.get("nom_lot");
    let typeEpi = url.searchParams.get("type_epi");
    let numeroSerie = url.searchParams.get("numero_serie");

    let message = url.searchParams.get("message");

    let groupeInput = $("#groupe");
    let lotInput = $("#lot");
    let typeInput = $("#type");

    /**
     * On affiche un message temporaire s'il y a eu une insertion d'un EPI juste avant
     */
    if (message !== undefined) {
        if (message === "insertionOk") {
            $("#cardInformation").addClass("card-body");
            $("#information").removeClass().addClass("alert alert-info font-weight-bold text-info text-center mt-2").html("Insertion de l'EPI réussie");
        } else if (message === "insertionOkLot") {
            $("#cardInformation").addClass("card-body");
            $("#information").removeClass().addClass("alert alert-info font-weight-bold text-info text-center mt-2").html("Insertion de l'EPI et du nouveau lot réussie");
        }
    }

    /**
     * On charge les types d'EPI
     */
    $.get(
        '/scripts_requests/select_all_typeepi.php',
        function (data) {
            let types = JSON.parse(data);
            let exists = false;
            for (let i = 0; i < types.length; i++) {
                let type = types[i]['libelle'];
                if (typeEpi === type) {
                    exists = true;
                    typeInput.append('<option selected="" value="' + type +
                        '">' + type +
                        '</option>');
                } else {
                    typeInput.append('<option value="' + type +
                        '">' + type +
                        '</option>');
                }
            }
            loadControlPoints(typeInput.val());
        },
        'text'
    );

    /**
     * On charge les structures/groupes en incluant celle du Stock avec
     */
    $.get(
        '/scripts_requests/select_all_groups.php',
        function (data) {
            let groupes = JSON.parse(data);
            let exists = false;
            for (let i = 0; i < groupes.length; i++) {
                let groupeBoucle = groupes[i]['nom_groupe'];
                if (groupe === groupeBoucle) {
                    exists = true;
                    groupeInput.append('<option selected="" value="' + groupeBoucle +
                        '">' + groupeBoucle +
                        '</option>');
                } else
                    groupeInput.append('<option value="' + groupeBoucle +
                        '">' + groupeBoucle +
                        '</option>');
            }
        },
        'text'
    ).then(
        /**
         * Ensuite, on charge les lots liésà la première structure
         */
        function () {
            $.post(
                '/scripts_requests/select_lots_by_group.php',
                {
                    groupe: groupeInput.val(),
                },
                function (data) {
                    let lots = JSON.parse(data);
                    let exists = false;
                    for (let i = 0; i < lots.length; i++) {
                        let lot = lots[i]['nom_lot'];
                        if (lot === lotName) {
                            exists = true;
                            lotInput.append('<option selected="" value="' + lot +
                                '">' + lot +
                                '</option>');
                        } else {
                            lotInput.append('<option value="' + lot +
                                '">' + lot +
                                '</option>');
                        }
                    }
                    if (lots.length === 0) {
                        lotInput.append("<option hidden selected='' disabled=''>Aucun lot pour ce groupe</option>");
                        $("#submitButton").attr("disabled", "");
                        $("#showButton").attr("disabled", "");
                    }
                },
                'text'
            ).then(
                /**
                 * Enfin, on charge les EPI liés au lot et au type d'EPI actuellement en première position dans les box
                 */
                function () {
                    $.post(
                        '/scripts_requests/select_epis_by_lot_typeepi.php',
                        {
                            type_epi: typeInput.val(),
                            lot: lotInput.val(),
                        },
                        function (data) {
                            let epis = JSON.parse(data);
                            let exists = false;
                            let epiInput = $("#epi");
                            for (let i = 0; i < epis.length; i++) {
                                let epi = epis[i]['numero_serie'];
                                if (epi === numeroSerie) {
                                    exists = true;
                                    epiInput.append("<option selected='' value='" + epi +
                                        "'>" + epi +
                                        "</option>");
                                } else {
                                    epiInput.append("<option value='" + epi +
                                        "'>" + epi +
                                        "</option>");
                                }
                            }
                            if (epis.length === 0) {
                                epiInput.append("<option hidden selected='' disabled=''>Aucun epi</option>");
                                $("#submitButton").attr("disabled", "");
                                $("#showButton").attr("disabled", "");
                                $("#mainRow").hide();
                                $("#cardInformation").addClass("card-body");
                                $("#information").removeClass().addClass("alert alert-info font-weight-bold text-center mb-2 pb-2 mt-2").html("Aucun EPI à vérifier");
                            }

                            loadInfosEpi();
                        },
                        'text'
                    )
                },
            )
        },
    );

    /**
     * On lie l'action de changement pour les groupes
     */
    groupeInput.on("change", function () {
        loadLotsByGroup()
    });

    /**
     * On lie l'action de changement pour les lots
     */
    lotInput.on("change", function () {
        loadEpiByGroupAndType()
    });

    /**
     * On lie l'action de changement pour les types
     */
    typeInput.on("change", function () {
        loadEpiByGroupAndType();
        loadControlPoints();
    })
}

/**
 * Fonction de chargement des EPI liés à un lot et à un type d'EPI
 */
function loadEpiByGroupAndType() {
    let nom_lot = $("#lot").val();
    let type_epi = $("#type").val();
    $.post(
        '/scripts_requests/select_epis_by_lot_typeepi.php',
        {
            type_epi: type_epi,
            lot: nom_lot,
        },
        function (data) {
            let epis = JSON.parse(data);
            let epiInput = $("#epi");
            epiInput.empty();
            for (let i = 0; i < epis.length; i++) {
                let epi = epis[i]['numero_serie'];
                epiInput.append("<option value='" + epi +
                    "'>" + epi +
                    "</option>");
            }
            if (epis.length === 0) {
                epiInput.append("<option hidden selected='' disabled=''>Aucun epi</option>");
                $("#submitButton").attr("disabled", "");
                $("#showButton").attr("disabled", "");

                $("#mainRow").hide();
                $("#cardInformation").addClass("card-body");
                $("#information").removeClass().addClass("alert alert-info font-weight-bold text-center mb-2 mt-2").html("Aucun EPI à vérifier");
            } else {
                $("#submitButton").removeAttr("disabled");
                $("#showButton").removeAttr("disabled");

                $("#mainRow").show();
                $("#cardInformation").removeClass();
                $("#information").removeClass().html("");

                loadControlPoints();
                $("#commentaires").val("");
                $("input:checked[name=etatRadio]").prop("checked", false);

                $.getScript("assets/js/utils.js", function () {
                    hideMessage();
                });
            }

            loadInfosEpi();
        },
        'text'
    )
}

/**
 * Fonction pour charger les infos de l'EPI
 */
function loadInfosEpi() {
    $.post(
        '/scripts_requests/select_infos_verif.php',
        {
            epi: $("#epi").val(),
        },
        function (data) {
            let result = JSON.parse(data);

            let today = new Date();
            let dd = today.getDate();
            let mm = today.getMonth() + 1;

            let yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = dd + '-' + mm + '-' + yyyy;

            if (result[0] !== undefined) {
                $("#date-mise-en-service").html(result[0]['date_mise_en_service']);
                $("#date-derniere-verif").html(result[0]['date_verification'] !== null ? result[0]['date_verification'] : "Non vérifié");
                $("#lien-pdf").prop("href", result[0]['url_pdf'] !== null ? location.protocol + "//" + result[0]['url_pdf'] : "#");
                $("#lien-pdf").html(result[0]['url_pdf'] !== null ? "Lien vers le PDF" : "");
                $("#date-verif-actuel").html(today);
            } else {
                $("#date-mise-en-service").html("");
                $("#date-derniere-verif").html("");
                $("#lien-pdf").prop("href", "#");
                $("#lien-pdf").html("");
                $("#date-verif-actuel").html(today);
            }
        },
        'text'
    );
}

/**
 * Fonction de chargement des lots liés à un groupe
 */
function loadLotsByGroup() {
    let groupe = $("#groupe").val();
    $.post(
        '/scripts_requests/select_lots_by_group.php',
        {
            groupe: groupe,
        },
        function (data) {
            let lots = JSON.parse(data);
            let exists = false;
            let lotInput = $("#lot");
            lotInput.empty();
            for (let i = 0; i < lots.length; i++) {
                let lot = lots[i]['nom_lot'];
                lotInput.append("<option value='" + lot +
                    "'>" + lot +
                    "</option>");
                exists = true;

            }
            if (!exists) {
                lotInput.append("<option hidden selected='' disabled=''>Aucun lot pour ce groupe</option>");
                $("#submitButton").attr("disabled", "");
                $("#showButton").attr("disabled", "");
            } else {
                $("#submitButton").removeAttr("disabled");
                $("#showButton").removeAttr("disabled");
            }
            loadEpiByGroupAndType()
        },
        'text'
    );
}

/**
 * Fonction qui va créer un PDF avec l'aperçu de la vérification
 * @param exportType Le type d'exportation
 */
function exportPDF(exportType) {
    let jsDoc = new jsPDF("p", "pt");
    let epi = $("#epi").val();
    let docLength = 0;

    /**
     * On récupère la date du jour
     * @type {Date}
     */
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;

    let yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    let nextYear = dd + "-" + mm + "-" + (today.getFullYear() + 1);
    today = dd + '-' + mm + '-' + yyyy;

    let currentPage = 1;

    jsDoc.setFontSize(18);
    jsDoc.setTextColor(40);
    jsDoc.setFontStyle('normal');

    // On écrit le titre du document
    let header = jsDoc.splitTextToSize("Vérification du " + today + " pour l'EPI " + epi, jsDoc.internal.pageSize.width - 40);
    jsDoc.text(header, jsDoc.internal.pageSize.width / 2, 50, null, null, 'center');

    jsDoc.text("Prochaine vérification le : " + nextYear, jsDoc.internal.pageSize.width / 2, 75, null, null, 'center');
    docLength = 100;

    // Ecriture des informations de l'EPI
    let result = undefined;
    let form = new FormData();
    form.append("numero_serie", epi);

    /**
     * On récupère les informations de l'EPI
     */
    $.ajax({
        url: '/scripts_requests/select_epi_by_numero_serie.php',
        data: form,
        contentType: false,
        processData: false,
        type: "POST",
        async: false,
        success: function (data, textStatus, jqXHR) {
            try {
                result = JSON.parse(data);
            } catch (e) {
                console.log(e);
            }
        }
    });

    /**
     * Si on récupère correctement les informations on set les infos dans le PDF
     */
    if (result !== undefined) {
        console.log("Ecriture fiche de vie");
        docLength += 20;
        jsDoc.setFontSize(16);

        // On crée le titre fiche de vie
        let head = jsDoc.splitTextToSize("Fiche de vie", jsDoc.internal.pageSize.width);
        jsDoc.text(head, jsDoc.internal.pageSize.width / 2, docLength, null, null, 'center');

        docLength += 30;

        /**
         * Si une image est définie on l'imprime sur le PDF
         */
        if (result['image'] !== null) {
            let image = new Image();

            image.src = location.protocol + "//" + result['image'];

            // On récupère l'extension
            let extension = result['image'].slice((result['image'].lastIndexOf(".") - 1 >>> 0) + 2);

            /*
            On récupère le ratio de l'image pour ne pas la déformer
             */
            let maxWidth = 210; // Max width for the image
            let maxHeight = 170;    // Max height for the image
            let ratio = 0;  // Used for aspect ratio
            let width = image.width;    // Current image width
            let height = image.height;  // Current image height

            /**
             * Selon le ratio on définit la table de l'image
             */
            if (width > maxWidth && width > height) {
                ratio = width / height;
                width = maxWidth;
                height = maxWidth / ratio;
            } else if (height > maxHeight && height > width) {
                ratio = height / width;
                width = maxHeight / ratio;
                height = maxHeight;
            } else {
                width = maxWidth;
                height = maxHeight;
            }

            /**
             * On écrit l'image
             */
            jsDoc.addImage(image, extension, 20, 100, width, height);

            jsDoc.setFontSize(12);

            // On écrit la fiche de vie
            jsDoc.text("Modèle : " + result['modele'], jsDoc.internal.pageSize.width / 2 - 50, docLength, null, null, 'left');

            docLength += 20;

            jsDoc.text("N° de série : " + result['numero_serie'], jsDoc.internal.pageSize.width / 2 - 50, docLength, null, null, 'left');
            jsDoc.text("Mise en service : " + result['date_mise_en_service'], jsDoc.internal.pageSize.width / 2 + 135, docLength, null, null, "left");

            docLength += 20;

            jsDoc.text("Lot : " + result['lot'], jsDoc.internal.pageSize.width / 2 - 50, docLength, null, null, 'left');
            jsDoc.text("Fin de vie : " + result['date_fin_de_vie'], jsDoc.internal.pageSize.width / 2 + 135, docLength, null, null, "left");

            docLength += 20;

            jsDoc.text("Code couleur : " + result['code_couleur'], jsDoc.internal.pageSize.width / 2 - 50, docLength, null, null, "left");
            jsDoc.text("Fabrication : " + result['annee_fabrication'], jsDoc.internal.pageSize.width / 2 + 135, docLength, null, null, 'left');

            docLength += 20;
            jsDoc.text("Marque : " + result['marque'], jsDoc.internal.pageSize.width / 2 - 50, docLength, null, null, 'left');


            docLength += 55;

        } else {

            jsDoc.setFontSize(12);

            // On écrit la fiche de vie
            jsDoc.text("Modèle : " + result['modele'], jsDoc.internal.pageSize.width / 2 - 90, docLength, null, null, 'left');

            docLength += 20;

            jsDoc.text("N° de série : " + result['numero_serie'], jsDoc.internal.pageSize.width / 2 - 90, docLength, null, null, 'left');
            jsDoc.text("Mise en service : " + result['date_mise_en_service'], jsDoc.internal.pageSize.width / 2 + 110, docLength, null, null, "left");

            docLength += 20;

            jsDoc.text("Lot : " + result['lot'], jsDoc.internal.pageSize.width / 2 - 90, docLength, null, null, 'left');
            jsDoc.text("Fin de vie : " + result['date_fin_de_vie'], jsDoc.internal.pageSize.width / 2 + 110, docLength, null, null, "left");

            docLength += 20;

            jsDoc.text("Code couleur : " + result['code_couleur'], jsDoc.internal.pageSize.width / 2 - 90, docLength, null, null, "left");
            jsDoc.text("Fabrication : " + result['annee_fabrication'], jsDoc.internal.pageSize.width / 2 + 110, docLength, null, null, 'left');

            docLength += 20;
            jsDoc.text("Marque : " + result['marque'], jsDoc.internal.pageSize.width / 2 - 90, docLength, null, null, 'left');

            docLength += 30;
        }
    }

    /**
     * On écrit les points de contrôle visuels s'il y en a
     */
    if ($("#cardVisuel").children().length !== 0) {
        jsDoc.setFontSize(16);
        docLength += 20;
        let titre = jsDoc.splitTextToSize("Vérifications visuelles", jsDoc.internal.pageSize.width - 40);
        jsDoc.text(titre, jsDoc.internal.pageSize.width / 2, docLength, null, null, 'center');

        let i = 1;
        jsDoc.setFontSize(12);

        /**
         * On récupère les champs sélectionnés de la partie visuelle
         */
        $("div#cardVisuel .control-point").each(function () {
            let controlName = $(this).find(".control-name").text().trim();
            let etat = $(this).find("input:checked").val();
            //let image = $(this).find(".span-choose-file").val();

            if (etat === undefined) {
                etat = "";
            }

            docLength += 40;

            /**
             * On regarde si on n'est pas arrivé au bout du PDF, dans ce cas on crée une nouvelle page
             */
            if (docLength >= jsDoc.internal.pageSize.height - 50) {
                docLength = 50;
                let nbPages = jsDoc.internal.getNumberOfPages() + 1;
                jsDoc.text(currentPage + "/" + nbPages, jsDoc.internal.pageSize.width - 20, jsDoc.internal.pageSize.height - 20, null, null, 'right');
                jsDoc.addPage();
                currentPage++;
            }

            // On écrit le point de contrôle
            let controle = jsDoc.splitTextToSize(controlName, jsDoc.internal.pageSize.width - 150);
            jsDoc.text(controle, 40, docLength, null, null, 'left');

            // On écrit l'état de la vérification pour ce point
            controle = jsDoc.splitTextToSize(etat, jsDoc.internal.pageSize.width - 40);
            jsDoc.text(controle, jsDoc.internal.pageSize.width - 40, docLength, null, null, 'right');

            i++;
        });

        docLength += 40;
    }

    /**
     * On écrit les points de contrôle et les états pour la partie fonctionnelle s'il y en a
     */
    if ($("#cardFonctionnel").children().length !== 0) {
        jsDoc.setFontSize(16);

        docLength += 20;

        /**
         * On regarde si on n'est pas arrivé au bout du PDF, dans ce cas on crée une nouvelle page
         */
        if (docLength >= jsDoc.internal.pageSize.height - 50) {
            docLength = 50;
            let nbPages = jsDoc.internal.getNumberOfPages() + 1;
            jsDoc.text(currentPage + "/" + nbPages, jsDoc.internal.pageSize.width - 20, jsDoc.internal.pageSize.height - 20, null, null, 'right');
            jsDoc.addPage();
            currentPage++;
        }

        let titre = jsDoc.splitTextToSize("Vérifications fonctionnelles", jsDoc.internal.pageSize.width - 40);
        jsDoc.text(titre, jsDoc.internal.pageSize.width / 2, docLength, null, null, 'center');

        let i = 1;
        jsDoc.setFontSize(12);

        /**
         * On récupère les champs sélectionnés de la partie fonctionnelle
         */
        $("div#cardFonctionnel .control-point").each(function () {
            let controlName = $(this).find(".control-name").text().trim();
            let etat = $(this).find("input:checked").val();
            //let image = $(this).find(".span-choose-file").val();

            if (etat === undefined) {
                etat = "";
            }

            docLength += 40;

            /**
             * On regarde si on n'est pas arrivé au bout du PDF, dans ce cas on crée une nouvelle page
             */
            if (docLength >= jsDoc.internal.pageSize.height - 50) {
                docLength = 50;
                let nbPages = jsDoc.internal.getNumberOfPages() + 1;
                jsDoc.text(currentPage + "/" + nbPages, jsDoc.internal.pageSize.width - 20, jsDoc.internal.pageSize.height - 20, null, null, 'right');
                jsDoc.addPage();
                currentPage++;
            }

            // On écrit le point de contrôle
            let controle = jsDoc.splitTextToSize(controlName, jsDoc.internal.pageSize.width - 150);
            jsDoc.text(controle, 40, docLength, null, null, 'left');

            // On écrit l'état pour ce point là
            controle = jsDoc.splitTextToSize(etat, jsDoc.internal.pageSize.width - 40);
            jsDoc.text(controle, jsDoc.internal.pageSize.width - 40, docLength, null, null, 'right');

            i++;
        });

        docLength += 40;

        /**
         * On regarde si on n'est pas arrivé au bout du PDF, dans ce cas on crée une nouvelle page
         */
        if (docLength >= jsDoc.internal.pageSize.height - 50) {
            docLength = 50;
            let nbPages = jsDoc.internal.getNumberOfPages() + 1;
            jsDoc.text(currentPage + "/" + nbPages, jsDoc.internal.pageSize.width - 20, jsDoc.internal.pageSize.height - 20, null, null, 'right');
            jsDoc.addPage();
            currentPage++;
        }
    }

    let isApte = $("input:checked[name=etatRadio]").val();
    let comments = $("#commentaires").val();

    // On écrit l'état final/global de la vérification
    let etatFinal = jsDoc.splitTextToSize("État final de la vérification : " + isApte, jsDoc.internal.pageSize.width - 40);
    jsDoc.text(etatFinal, jsDoc.internal.pageSize.width / 2, docLength, null, null, 'center');

    docLength += 20;

    /**
     * On regarde si on n'est pas arrivé au bout du PDF, dans ce cas on crée une nouvelle page
     */
    if (docLength >= jsDoc.internal.pageSize.height - 50) {
        docLength = 50;
        let nbPages = jsDoc.internal.getNumberOfPages() + 1;
        jsDoc.text(currentPage + "/" + nbPages, jsDoc.internal.pageSize.width - 20, jsDoc.internal.pageSize.height - 20, null, null, 'right');
        jsDoc.addPage();
        currentPage++;
    }

    // On écrit les commentaires
    let commentaires = jsDoc.splitTextToSize(comments, jsDoc.internal.pageSize.width - 75);
    jsDoc.text(commentaires, 40, docLength, null, null, 'left');

    if (comments.length < 100)
        docLength += 40;
    else if (comments.length < 200)
        docLength += 50;
    else if (comments.length < 300)
        docLength += 60;
    else
        docLength += 70;

    /**
     * On regarde si on n'est pas arrivé au bout du PDF, dans ce cas on crée une nouvelle page
     */
    if (docLength >= jsDoc.internal.pageSize.height - 50) {
        docLength = 50;
        let nbPages = jsDoc.internal.getNumberOfPages() + 1;
        jsDoc.text(currentPage + "/" + nbPages, jsDoc.internal.pageSize.width - 20, jsDoc.internal.pageSize.height - 20, null, null, 'right');
        jsDoc.addPage();
        currentPage++;
    }

    let session = undefined;

    /**
     * On récupère la session pour écrire le vérificateur
     */
    $.ajax({
        url: '/scripts_requests/select_session.php',
        data: form,
        contentType: false,
        processData: false,
        type: "POST",
        async: false,
        success: function (data, textStatus, jqXHR) {
            session = JSON.parse(data);
        }
    });

    /**
     * On regarde si la session est bien retournée
     */
    if (session !== undefined) {
        // On écrit le vérificateur
        let texte = jsDoc.splitTextToSize("Vérificateur : " + session['nom'] + " " + session["prenom"] + " " + session['mail'] + " - " + session['certificat'], jsDoc.internal.pageSize.width - 40);
        jsDoc.text(texte, 40, docLength, null, null, 'left');
        docLength += 20;

        /**
         * On regarde si on n'est pas arrivé au bout du PDF, dans ce cas on crée une nouvelle page
         */
        if (docLength >= jsDoc.internal.pageSize.height - 50) {
            docLength = 50;
            let nbPages = jsDoc.internal.getNumberOfPages() + 1;
            jsDoc.text(currentPage + "/" + nbPages, jsDoc.internal.pageSize.width - 20, jsDoc.internal.pageSize.height - 20, null, null, 'right');
            jsDoc.addPage();
            currentPage++;
        }
    }

    // On demande la signature à la fin du PDF
    jsDoc.text("Signature du vérificateur : ", 40, docLength, null, null, 'left');
    jsDoc.text("Signature de l'utilisateur : ", jsDoc.internal.pageSize.width - 40, docLength, null, null, "right");

    jsDoc.setProperties({
        title: "Vérification_" + today + "_EPI_" + epi + ".pdf",
    });

    jsDoc.text(currentPage + "/" + jsDoc.internal.getNumberOfPages(), jsDoc.internal.pageSize.width - 20, jsDoc.internal.pageSize.height - 20, null, null, 'right');

    /**
     * Selon le type d'exportation on sauvegarde le PDF dans les dossiers du serveur ou dans les dossiers de l'utilisateur
     */
    if (exportType)
        jsDoc.save("Vérification_" + today + "_EPI_" + epi + ".pdf");
    else
        jsDoc = jsDoc.output("blob");
    return jsDoc;
}