$(document).ready(function () {

    /**
     * On définit l'action du changement de mot de passe
     */
    $("#submitButton").on("click", function () {
        changePassword();
    });

    /**
     * Lorsque l'utilisateur tape sur le clavier on vérifie si les deux mots de passe sont identiques
     */
    $("#new_password_2").on('keyup', function () {
        let password = $("#new_password").val();
        let password2 = $("#new_password_2").val();
        samePassword(password, password2);
    });

    /**
     * Lorsque l'utilisateur tape sur le clavier on vérifie si les deux mots de passe sont identiques
     */
    $("#new_password").on("keyup", function () {
        let password = $("#new_password").val();
        let password2 = $("#new_password_2").val();
        samePassword(password, password2);

    });
});

/**
 * Fonction de changement de mot de passe
 * @returns {boolean}
 */
function changePassword() {
    event.preventDefault();
    let oldPassword = $("#old_password").val();
    let newPassword = $("#new_password").val();
    let newPasswordRepeated = $("#new_password_2").val();

    /**
     * On vérifie que tous les mots de passes soient saisis
     */
    if (newPassword === "" || newPasswordRepeated === "" || oldPassword === "") {
        displayErrorMessage("L'un des champs mot de passe n'a pas été saisi");
        return false;
    }

    /**
     * On compare les deux champs pour le nouveau mot de passe
     */
    if (!samePassword(newPassword, newPasswordRepeated))
        return false;

    /**
     * On fait un appel ajax pour modifier le mot de passe
     */
    $.post(
        '/scripts_requests/update_password.php',
        {
            old_password: oldPassword,
            new_password: newPasswordRepeated,
        },
        function (data) {
            try {
                let insertion_ok = JSON.parse(data);
                if (insertion_ok['success'] === true) {
                    displayInfoMessage("Votre mot de passe a bien été modifié");
                    window.location.href = "/profil?changement-mdp=success";
                } else {
                    displayInfoMessage("Ancien mot de passe incorrect, modification impossible");
                }
            } catch (e) {
                displayInfoMessage("Erreur de connexion à la base de données");
                console.log(e);
            }
        },
        'text'
    )
}