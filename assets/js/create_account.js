$(document).ready(function () {
    /**
     * On définit l'action d'ajout d'un vérificateur
     */
    $("#submitButton").on("click", function () {
        createAccount();
    });

    /**
     * On compare les deux mots de passe lorsqu'une touche est saisie
     */
    $("#password_2").on('keyup', function () {
        let password = $("#password").val();
        let passwordRepeated = $("#password_2").val();
        samePassword(password, passwordRepeated);
    });

    /**
     * On compare les deux mots de passe lorsqu'une touche est saisie
     */
    $("#password").on("keyup", function () {
        let password = $("#password").val();
        let passwordRepeated = $("#password_2").val();
        samePassword(password, passwordRepeated);
    });

    /**
     * On vérifie que le mail soit bien un mail au fur et à mesure de la saisie
     */
    $("#mail").on("keyup", function () {
        let validEmail = validateEmail($("#mail").val());
        if (!validEmail) {
            displayErrorMessage("L'email saisi est invalide");
        } else {
            hideMessage();
        }
    });

    let oldValuePrenom = "";
    $("#prenom").on("keyup", function () {
        let prenom = $("#prenom").val();
        if (prenom !== "" && oldValuePrenom !== prenom) {
            let firstLetter = prenom.charAt(0).toUpperCase();
            let string = prenom.slice(1);

            oldValuePrenom = firstLetter + string;

            $("#prenom").val(firstLetter + string);
        }
    });

    let oldValueNom = "";
    $("#nom").on("keyup", function () {
        let prenom = $("#nom").val();
        if (prenom !== "" && oldValueNom !== prenom) {
            let firstLetter = prenom.charAt(0).toUpperCase();
            let string = prenom.slice(1);

            oldValueNom = firstLetter + string;

            $("#nom").val(firstLetter + string);
        }
    });
});

/**
 * Fonction d'insertion d'un vérificateur
 * @returns {boolean}
 */
function createAccount() {
    event.preventDefault();
    let certificat = $("#certificat").val();
    let nom = $("#nom").val();
    let prenom = $("#prenom").val();
    let mail = $("#mail").val();
    let password = $("#password").val();
    let passwordRepeated = $("#password_2").val();

    /**
     * On vérifie que tous les champs soient saisis
     */
    if (certificat === "" || nom === "" || prenom === "" || mail === "") {
        displayErrorMessage("L'un des champs obligatoire n'a pas été saisi");
        return false;
    }

    /**
     * On vérifie que les mots de passe soient saisis
     */
    if (password === "" || passwordRepeated === "") {
        displayErrorMessage("L'un des champs mot de passe n'a pas été saisi");
        return false;
    }

    /**
     * On compare les deux mots de passes
     */
    if (!samePassword(password, passwordRepeated))
        return false;

    /**
     * On valide l'email
     */
    if (!validateEmail(mail)) {
        displayErrorMessage("L'email saisi est invalide");
        return false;
    }

    /**
     * On fait un appel ajax pour l'ajout du vérificateur
     */
    $.post(
        '/scripts_requests/insert_verificateur.php',
        {
            certificat: certificat,
            nom: nom,
            prenom: prenom,
            mail: mail,
            password: password
        },
        function (data) {
            try {
                let insertion_ok = JSON.parse(data);
                if (insertion_ok['success'] === true) {
                    displayInfoMessage("Insertion du vérificateur réussie");
                    window.location.href = "/profil?creation=success";
                } else if (insertion_ok['success'] === "Duplicata")
                    displayErrorMessage("Insertion du vérificateur impossible car le certificat ou le mail sont déjà utilisés pour un vérificateur");
                else
                    displayErrorMessage("Insertion du vérificateur impossible");

            } catch (e) {
                displayErrorMessage("Erreur de connexion à la base de données");
                console.log(e);
            }
        },
        'text'
    )
}