$(document).ready(function () {
    /**
     * On récupère les noms des lots pour remplir la combobox des lots
     */
    $.get(
        '/scripts_requests/select_all_lots.php',
        {},
        function (data) {
            let lots = JSON.parse(data);
            for (let index = 0; index < lots.length; index++) {
                $("#btnLot").append("<option value='" + lots[index]['nom_lot'] +
                    "'>" + lots[index]['nom_lot'] +
                    "</option>");
            }
        },
        'text'
    );

    /**
     * On définit l'action lorsque l'utilisateur clique sur une ligne du tableau
     */
    $('#table-epi tbody').on('click', 'tr', function () {
        /**
         * On vérifie qu'il y a bien des enregistrements dans le tableau
         */
        if ($(this).find('td').eq(0).text() !== "Aucun EPI à afficher") {

            // On récupère le numéro de série de l'EPI sélectionné
            let numeroSerie = $(this).find('td').eq(1).text();

            ///On complète des champs du modal
            $('#EPI_ID').replaceWith("<b>Numéro de série de l'EPI : </b>" +numeroSerie +'<br>');
            $('#modele').replaceWith("<b>Modèle : </b>" + $(this).find('td').eq(0).text() + " /<b> Marque : </b>" + $(this).find('td').eq(2).text() +'<br>' + '<br>');
            // On affiche le modal
            $('#exampleModalPreview').modal('show');



            /**
             * On définit l'action lorsque l'utilisateur va cliquer sur le bouton ajouter
             */
            $('#submitButton').on('click', function () {

                // On récupère le nom du lot
                let lotName = $("#btnLot").val();

                console.log(lotName);

                if (lotName === null) {
                    $.getScript("assets/js/utils.js", function () {
                       customErrorMessage("#errorMessage","Veuillez saisir un lot auquel ajouter l'EPI");
                    });
                    return false;
                }

                /**
                 * On appelle un ajax afin de modifier dans la base de données le nom du lot de l'EPI
                 */
                $.post(
                    '/scripts_requests/update_epi_set_lot.php',
                    {
                        nom_lot: lotName,
                        numero_serie: numeroSerie
                    },
                    function (data) {
                        try {
                            let insertion_ok = JSON.parse(data);

                            /**
                             * Si la modification a réussie
                             */
                            if (insertion_ok['success'] === true) {

                                /**
                                 * On affiche un message temporaire
                                 */
                                $.getScript("assets/js/utils.js", function () {
                                    displayTemporaryInfoMessage("Insertion de l'EPI " + numeroSerie + " dans le lot " + lotName + " réussie")
                                });

                                /**
                                 * On met à jour la table des EPI du magasin
                                 */
                                $('#table-epi').DataTable({
                                    "destroy": true,
                                    "ajax": {
                                        "url": "/scripts_requests/dotation_table.php",
                                        "dataSrc": "",
                                        "data": {
                                            "lot": "Stock",
                                            "groupe": "Stock",
                                        },
                                        "type": 'POST',
                                    },
                                    "dom": "ftipr",
                                    "pagingType": "simple",
                                    "searching": true,
                                    "language": {
                                        "emptyTable": "Aucun EPI à afficher",
                                        "info": "_START_ à _END_ sur _TOTAL_ EPI affichés",
                                        "infoEmpty": "",
                                        "loadingRecords": "Chargement de la table des EPI ...",
                                        "processing": "Recherche deS EPI ...",
                                        "infoFiltered": "(Filtrées parmi les _MAX_ EPI)",
                                        "lengthMenu": "(Affichage de _MENU_ entrées)",
                                        "zeroRecords": "Pas d'EPI correspondants à la recherche",
                                        "search": "",
                                        "paginate": {
                                            "next": "Suivant",
                                            "previous": "Précédent"
                                        },
                                    },
                                    "autoFill": true,
                                    "paging": true,
                                    "lengthChange": false,
                                });

                                // On cache le modal
                                $('#exampleModalPreview').modal('hide');
                            } else
                                $.getScript("assets/js/utils.js", function () {
                                    displayErrorMessage("Insertion de l'EPI " + numeroSerie + " dans le lot " + lotName + " non réussie")
                                });
                        } catch (e) {
                            $.getScript("assets/js/utils.js", function () {
                                displayErrorMessage("Erreur de connexion à la base de données")
                            });
                            console.log(e);
                        }
                    },
                    'text'
                );
            })
        }
    });
});

