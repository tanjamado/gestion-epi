

$(document).ready(function () {

    /**
     * On charge la table des lots
     */
    $('#table-lots').DataTable({
        "ajax": {
            "url": "/scripts_requests/select_all_lots_without_stock.php",
            "dataSrc": "",
            "type": 'get',
        },
        "dom": "ftipr",
        "pagingType": "simple",
        "searching": true,
        "language": {
            "emptyTable": "Aucun lot à afficher",
            "info": "_START_ à _END_ sur _TOTAL_ lots affichés",
            "infoEmpty": "",
            "loadingRecords": "Chargement de la table des lots ...",
            "processing": "Recherche de lots ...",
            "infoFiltered": "(Filtrées parmi les _MAX_ lot(s))",
            "lengthMenu": "Affichage de _MENU_ entrées",
            "zeroRecords": "Pas de lots correspondants à la recherche",
            "search": "",
            "paginate": {
                "next": "Suivant",
                "previous": "Précédent"
            },
        },
        "autoFill": true,
        "paging": true,
        "lengthChange": false,
    });

    /**
     * Lorsque l'on clique sur une ligne, on charge la page avec les EPI du lot
     */
    $('#table-lots tbody').on('click', 'tr', function () {
        let nom_lot = $(this).find('td').eq(0).text();
        let nom_groupe = $(this).find('td').eq(2).text();
        if (nom_lot !== "Aucun lot à afficher")
            displayLot(nom_lot, nom_groupe);
    });

    $("#submitButton").on("click", function () {
        putEpiStock();
    });


});

/**
 * Fonction qui renvoie vers la page d'affichage des EPI
 * @param nom_lot
 * @param nom_groupe
 */
function displayLot(nom_lot, nom_groupe) {
    window.location.href = "/dotation?nom_lot=" + nom_lot + "&nom_groupe=" + nom_groupe;
}